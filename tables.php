<!-- <!doctype html> -->
<html lang="en">
<head>
    <title>PICO</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bsadmin.css">
    <link rel="stylesheet" href="css/datatables.css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<style>
    body{
        font-family: "Noto Sans Thai", 'Arimo', ystem-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Fira Sans", "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 1.5rem;
        color: #212121;
    }
    .pico-content{
        width: 1090px;
        height: 100%;
        background-color:#ffffff;
        position: relative;
        top: 0px;
        left: 10px;
        z-index: 1;
    }
    .pico-dashboard{
        /* padding: 0px 0px 20px 500px; */
        font-size: 40px;
    }
    .pico-node-red-status{
        position: fixed;
        font-size:30px;
        right: 150px;
        top: 10px;
        z-index:3;
        color:rgb(47, 116, 8);
    }
    .pico-text-status{
        position: fixed;
        right: 90px;
        top: 14px;
        font-size: 20px;
        text-align: right;
    }
</style>
<body style="background-color:rgb(255, 255, 255)">

<nav class="navbar navbar-expand navbar-dark bg-dark" style = "font-size: 30px;">
    <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>

    <a class="navbar-brand" href="#"></a>
    <!-- <div class='pico-node-red-status'><i class="fa fa-circle"> <p class='pico-text-status'>Active  </p></i></div> -->
</nav>

<div class="d-flex" style = "height:1000px;">
    <nav class="sidebar bg-dark toggled">
        <ul class="list-unstyled">
            <li><a href="dashboard.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
            <li><a href="tables.php"><i class="fa fa-fw fa-table"></i> Record</a></li>
            <li><a href="control.php"><i class="fa fa-fw fa-hand-o-up"></i> Control</a></li>
            <li><a href="settings.php"><i class="fa fa-fw fa-gears"></i> Setting</a></li>
            <li><a href="login/index_security.php"><i class="fa fa-fw fa-lock"></i> Security</a></li>
            <li><a href="safemode.php"><i class="fa fa-fw fa-hand-o-up"></i> Safe Mode</a></li>
        </ul>
        </ul>
    </nav>
    <div class='pico-content'>
        <div class='content'>

            <div class='row'>
                <div class='col-sm-12'>
                    <center>
                        <div class='pico-dashboard'>
                            Record
                        </div>
                    </center>
                </div>
            </div>

            <div class='row'>
                <div class='col-sm-12'>
                    <div class="container-fluid" id="head_t">
                        <table class="table  table-bordered " id="test_table">
                            <thead align="center">
                                <tr class="tr_head tr_color" >
                                    <th scope="col" width="20%">Time</th>
                                    <th scope="col" width="10%">pH</th>
                                    <th scope="col" width="10%">pH status</th>
                                    <th scope="col" width="10%">DO</th>
                                    <th scope="col" width="10%">DO status</th>
                                    <th scope="col" width="10%">EC</th>
                                    <th scope="col" width="10%">EC status</th>
                                    <th scope="col" width="10%">Turbidity</th>
                                    <th scope="col" width="10%">Turbidity status</th>
                                    <th scope="col" width="10%">NH4_H</th>
                                    <th scope="col" width="10%">NH4_H status</th>
                                    <th scope="col" width="10%">TDS</th>
                                    <th scope="col" width="10%">TDS status</th>
                                    <th scope="col" width="10%">Tempareture</th>
                                    <th scope="col" width="10%">Tempareture status</th>
                                    <th scope="col" width="10%">Salinity</th>
                                    <th scope="col" width="10%">Salinity status</th>
                                </tr>
                            </thead>
                            <tbody id="body_t" align="center">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bsadmin.js"></script>
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="js/datatables.js"></script>
                            <!-- <div class='col-md-2'>
                                <button type = 'button' class="btn btn-primary" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                                <button type = 'button'  class="btn btn-primary" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                            </div> -->
</body>
<script type="text/javascript"> 

// let tranfer_data = new WebSocket("ws://localhost:1880/wt/config");
// let this_onload = new WebSocket("ws://localhost:1880/wt/onload");
let pagelogin = new WebSocket("ws://localhost:1880/wt/pagelogin");
let data_wt = new WebSocket("ws://localhost:1880/wt/data_wt");
// let tranfer_data = new WebSocket("ws://localhost:1880/wt/config");
// let this_onload = new WebSocket("ws://localhost:1880/wt/onload");

$( document ).ready(function() {
    // onload_data();
    // sock_to_update();
    page_login();
    connect_data_wt();
    document.body.style.zoom = "90%";
});
function connect_data_wt(){
    data_wt.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
        data_wt.send(1);
    };

    data_wt.onmessage = function(event) {
        console.log(event.data);
        table_C(event.data);
    };

    data_wt.onclose = function(event) {
        
    };

    data_wt.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}
function page_login(){
    pagelogin.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    pagelogin.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.page_login){
            location.replace("login/index.php");
        }
    };

    pagelogin.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    pagelogin.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}
function sock_to_update(){
    tranfer_data.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    tranfer_data.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.result){
            // alert("บันทึกข้อมูลเรียบร้อยแล้ว")
        }
    };

    tranfer_data.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    tranfer_data.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}

function onload_data(){
    this_onload.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
        request_data();
    };

    this_onload.onmessage = function(event) {
        var obj = JSON.parse(event.data);
        console.log(obj);
        $("#title").text(obj[0].title);
    };

    this_onload.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    this_onload.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };

}

function request_data(){
    setTimeout(function() {
    }, 1000);
    this_onload.send('{"cmd":"onload"}');
    
}

function onclick_(){
    if ($("#number_").val() == "" || $("#title_").val() == ""){
        alert("กรอกข้อมูลให้ครบ")
    }else{
        // tranfer_data.send('{"_id":1,"water_pump_time":'+$("#number_").val()+',"min_is":23,"max_is":124,"set_temp":1000,"title":"'+$("#title_").val()+'","alam_Temperature_lo":"'+$("#alam_Temperature_lo").val()+'","alam_Temperature_lolo":"'+$("#alam_Temperature_lolo").val()+'","alam_Temperature_hl":"'+$("#alam_Temperature_hi").val()+'","alam_Temperature_hihi":"'+$("#alam_Temperature_hihi").val()+'"}');
    }
}
function up(id){
    var num = $("#"+id).val()
    num++;
    $("#"+id).val(num);
}
function down(id){
    var num = $("#"+id).val()
    if(num != 0){
        num--;
    }
    $("#"+id).val(num);
}
function table_C(decode_){
    var decode = JSON.parse(decode_);
    console.log(decode);
    for(var i=0; i<decode.length; i++){
        var date = moment(decode[i].RTime,'YYYY-MM-DD HH:mm:ss').add(7,"hours");
        $('#body_t').append(
            '<tr class="tr_body">'+
            '<td width="20%">'+date.format('YYYY-MM-DD HH:mm:ss')+'</td>'+
            '<td width="10%">'+decode[i].pH+'</td>'+
            '<td width="10%">'+decode[i].pH_status+'</td>'+
            '<td width="10%">'+decode[i].DO+'</td>'+
            '<td width="10%">'+decode[i].DO_status+'</td>'+
            '<td width="10%">'+decode[i].EC+'</td>'+
            '<td width="10%">'+decode[i].EC_status+'</td>'+
            '<td width="10%">'+decode[i].Turbidity+'</td>'+
            '<td width="10%">'+decode[i].Turbidity_status+'</td>'+
            '<td width="10%">'+decode[i].NH4_N+'</td>'+
            '<td width="10%">'+decode[i].NH4_N_status+'</td>'+
            '<td width="10%">'+decode[i].TDS+'</td>'+
            '<td width="10%">'+decode[i].TDS_status+'</td>'+
            '<td width="10%">'+decode[i].Temperature+'</td>'+
            '<td width="10%">'+decode[i].Temperature_status+'</td>'+
            '<td width="10%">'+decode[i].Salinity+'</td>'+
            '<td width="10%">'+decode[i].Salinity_status+'</td>'+
            '</tr>'
            
        );
    }
    $('#test_table').DataTable({});
}
</script>
</html>