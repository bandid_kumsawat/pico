<!-- modal keypad -->
<!-- title -->
<div class="modal fade bd-example-modal-lg-title" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
            <div id="keyboard_title"></div>
      </div>
    </div>
</div>

<!-- station id -->
<div class="modal fade bd-example-modal-lg-status-id" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
            <div id="keyboard_station_id"></div>
      </div>
    </div>
</div>


<!-- ph -->
<div class="modal fade bd-example-modal-lg-ph-gauge-name" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_ph_gauge_name"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-ph-gauge-unit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_ph_gauge_unit"></div>
        </div>
    </div>
</div>
<!-- end -->

<!-- DO -->
<div class="modal fade bd-example-modal-lg-DO-gauge-name" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_DO_gauge_name"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-DO-gauge-unit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_DO_gauge_unit"></div>
        </div>
    </div>
</div>
<!-- end -->
<!-- EC -->
<div class="modal fade bd-example-modal-lg-EC-gauge-name" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_EC_gauge_name"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-EC-gauge-unit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_EC_gauge_unit"></div>
        </div>
    </div>
</div>
<!-- end -->
<!-- Turbidity -->
<div class="modal fade bd-example-modal-lg-Turbidity-gauge-name" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Turbidity_gauge_name"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-Turbidity-gauge-unit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Turbidity_gauge_unit"></div>
        </div>
    </div>
</div>
<!-- end -->
<!-- NH4_N -->
<div class="modal fade bd-example-modal-lg-NH4_N-gauge-name" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_NH4_N_gauge_name"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-NH4_N-gauge-unit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_NH4_N_gauge_unit"></div>
        </div>
    </div>
</div>
<!-- end -->
<!-- TDS -->
<div class="modal fade bd-example-modal-lg-TDS-gauge-name" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_TDS_gauge_name"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-TDS-gauge-unit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_TDS_gauge_unit"></div>
        </div>
    </div>
</div>
<!-- end -->
<!-- Temperature -->
<div class="modal fade bd-example-modal-lg-Temperature-gauge-name" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Temperature_gauge_name"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-Temperature-gauge-unit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Temperature_gauge_unit"></div>
        </div>
    </div>
</div>
<!-- end -->
<!-- Salinity -->
<div class="modal fade bd-example-modal-lg-Salinity-gauge-name" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Salinity_gauge_name"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-Salinity-gauge-unit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Salinity_gauge_unit"></div>
        </div>
    </div>
</div>
<!-- end -->
<!-- Line -->
<!-- token -->
<div class="modal fade bd-example-modal-lg-keyboard-line-token" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_line_token"></div>
        </div>
    </div>
</div>

<!-- lolo -->
<div class="modal fade bd-example-modal-lg-keyboard-line-lolo-message" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_line_lolo_message"></div>
        </div>
    </div>
</div>

<!-- lo -->
<div class="modal fade bd-example-modal-lg-keyboard-line-lo-message" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_line_lo_message"></div>
        </div>
    </div>
</div>

<!-- hi -->
<div class="modal fade bd-example-modal-lg-keyboard-line-hi-message" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_line_hi_message"></div>
        </div>
    </div>
</div>

<!-- hihi -->
<div class="modal fade bd-example-modal-lg-keyboard-line-hihi-message" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_line_hihi_message"></div>
        </div>
    </div>
</div>
<!-- end -->



<!-- ===================================================================================================================================================== -->
<!-- ===================================================================================================================================================== -->
<!-- ===================================================================================================================================================== -->
<!-- ===================================================================================================================================================== -->

<!-- this is pH modal  -->
<div class="modal fade bd-example-modal-lg-keyboard-pH-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_pH_1"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-keyboard-pH-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_pH_2"></div>
        </div>
    </div>
</div>


<div class="modal fade bd-example-modal-lg-keyboard-pH-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_pH_3"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-keyboard-pH-4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_pH_4"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-keyboard-pH-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_pH_5"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-pH-6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_pH_6"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-pH-7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_pH_7"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-pH-8" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_pH_8"></div>
        </div>
    </div>
</div>
<!-- end  -->
<!-- this is DO modal -->
<div class="modal fade bd-example-modal-lg-keyboard-DO-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_DO_1"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-DO-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_DO_2"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-DO-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_DO_3"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-DO-4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_DO_4"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-DO-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_DO_5"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-DO-6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_DO_6"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-DO-7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_DO_7"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-DO-8" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_DO_8"></div>
        </div>
    </div>
</div>
<!-- end  -->
<!-- this is EC modal -->
<div class="modal fade bd-example-modal-lg-keyboard-EC-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_EC_1"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-EC-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_EC_2"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-EC-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_EC_3"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-EC-4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_EC_4"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-EC-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_EC_5"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-EC-6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_EC_6"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-EC-7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_EC_7"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-EC-8" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_EC_8"></div>
        </div>
    </div>
</div>
<!-- end  -->
<!-- this is Turbidity modal -->
<div class="modal fade bd-example-modal-lg-keyboard-Turbidity-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Turbidity_1"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Turbidity-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Turbidity_2"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Turbidity-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Turbidity_3"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Turbidity-4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Turbidity_4"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Turbidity-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Turbidity_5"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Turbidity-6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Turbidity_6"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Turbidity-7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Turbidity_7"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Turbidity-8" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Turbidity_8"></div>
        </div>
    </div>
</div>
<!-- end  -->
<!-- this is NH4_N modal -->
<div class="modal fade bd-example-modal-lg-keyboard-NH4-N-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_NH4_N_1"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-NH4-N-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_NH4_N_2"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-NH4-N-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_NH4_N_3"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-NH4-N-4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_NH4_N_4"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-NH4-N-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_NH4_N_5"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-NH4-N-6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_NH4_N_6"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-NH4-N-7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_NH4_N_7"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-NH4-N-8" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_NH4_N_8"></div>
        </div>
    </div>
</div>
<!-- end  -->
<!-- this is TDS modal -->
<div class="modal fade bd-example-modal-lg-keyboard-TDS-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_TDS_1"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-TDS-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_TDS_2"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-TDS-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_TDS_3"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-TDS-4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_TDS_4"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-TDS-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_TDS_5"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-TDS-6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_TDS_6"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-TDS-7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_TDS_7"></div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg-keyboard-TDS-10" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_TDS_10"></div>
        </div>
    </div>
</div>


<!-- end  -->
<!-- this is Temperature modal -->
<div class="modal fade bd-example-modal-lg-keyboard-Temperature-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Temperature_1"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Temperature-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Temperature_2"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Temperature-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Temperature_3"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Temperature-4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Temperature_4"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Temperature-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Temperature_5"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Temperature-6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Temperature_6"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Temperature-7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Temperature_7"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Temperature-8" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Temperature_8"></div>
        </div>
    </div>
</div>
<!-- end  -->
<!-- this is Salinity modal -->
<div class="modal fade bd-example-modal-lg-keyboard-Salinity-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Salinity_1"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Salinity-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Salinity_2"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Salinity-3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Salinity_3"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Salinity-4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Salinity_4"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Salinity-5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Salinity_5"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Salinity-6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Salinity_6"></div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg-keyboard-Salinity-7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="keyboard_Salinity_7"></div>
        </div>
    </div>
</div>
<!-- ===================================================================================================================================================== -->
<!-- ===================================================================================================================================================== -->
<!-- ===================================================================================================================================================== -->
<!-- ===================================================================================================================================================== -->
<!-- end modal keypad -->