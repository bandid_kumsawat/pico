
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config gauge_NH4_N <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title'class='pico-setting'>
                Set Param NH4_N
            </div>
        </center>
    </div>
</div>
<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm NH4_N [LOLO] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_NH4_N_lolo" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-NH4-N-1" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_NH4_N_lolo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_NH4_N_lolo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div class = 'col-md-4'>
        <div hidden class='checkbox'>
            <label>
                <input chNH4_Nked type='checkbox'  value = '1' id = 'status_conf_alam_NH4_N_lolo'>
            </label>
        </div>
    </div>
</div>

<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm NH4_N [LO] : </b>
        </div>
    </div>

    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_NH4_N_lo" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-NH4-N-2"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_NH4_N_lo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_NH4_N_lo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>

    <!-- status enable and disable -->
    <div  hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_NH4_N_lo'>
            </label>
        </div>
    </div>

    
</div>




<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm NH4_N [HI] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_NH4_N_hi" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-NH4-N-3"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_NH4_N_hi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_NH4_N_hi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden  class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input chNH4_Nked type='checkbox'  value = '1' id = 'status_conf_alam_NH4_N_hi'>
            </label>
        </div>
    </div>
</div>


<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm NH4_N [HIHI] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_NH4_N_hihi" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-NH4-N-4"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_NH4_N_hihi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_NH4_N_hihi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input chNH4_Nked type='checkbox'  value = '1' id = 'status_conf_alam_NH4_N_hihi'>
            </label>
        </div>
    </div>
</div>
<!-- -------------------------------------------------------- -->
<hr>
<div hidden class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div  class='pico-setting'>
                Set Adjust NH4_N Calculater
            </div>
        </center>
    </div>
</div>
<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>value min : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  value =  "790" type = "hidden" class="form-control"  id = "Adjust_NH4_N_calculater" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-NH4-N-5"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('Adjust_NH4_N_calculater')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('Adjust_NH4_N_calculater')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>
<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>value max : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  value =  "3950" type = "hidden" class="form-control"  id = "Adjust_NH4_N_calculater_max" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-NH4-N-6"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('Adjust_NH4_N_calculater_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('Adjust_NH4_N_calculater_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>
<hr>
<!-- -------------------------------------------------------- -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title' class='pico-setting'>
                Set NH4_N Gauge
            </div>
        </center>
    </div>
</div>

<div class='row'>
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Name  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" id = "gauge_NH4_N_name" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-NH4_N-gauge-name"/>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>min : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "gauge_NH4_N_min" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-NH4-N-7"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('gauge_NH4_N_min')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_NH4_N_min')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>max : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "gauge_NH4_N_max" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-NH4-N-8"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('gauge_NH4_N_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_NH4_N_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>

<div class='row' style = "margin-top:20px;" >
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>unit  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" value = " " type = "text" class="form-control" id = "gauge_NH4_N_unit" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-NH4_N-gauge-unit"/>
    </div>
</div>
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config gauge_NH4_N <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->

<script type="text/javascript"> 
    $( document ).ready(function() {
        $('#status_conf_alam_NH4_N_lo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_NH4_N_lolo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_NH4_N_hi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_NH4_N_hihi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#keyboard_NH4_N_gauge_name').jkeyboard({
            // layout: "english",
            input: $('#gauge_NH4_N_name'),
        });
        $('#keyboard_NH4_N_gauge_unit').jkeyboard({
            // layout: "english",
            input: $('#gauge_NH4_N_unit'),
        });
        // ============================================================
        // ============================================================
        // ============================================================
        // ============================================================
        $('#keyboard_NH4_N_1').jkeyboard({
            input: $('#alam_NH4_N_lolo'),
        });
        $('#keyboard_NH4_N_2').jkeyboard({
            input: $('#alam_NH4_N_lo'),
        });
        $('#keyboard_NH4_N_3').jkeyboard({
            input: $('#alam_NH4_N_hi'),
        });
        $('#keyboard_NH4_N_4').jkeyboard({
            input: $('#alam_NH4_N_hihi'),
        });
        $('#keyboard_NH4_N_5').jkeyboard({
            input: $('#Adjust_NH4_N_calculater'),
        });
        $('#keyboard_NH4_N_6').jkeyboard({
            input: $('#Adjust_NH4_N_calculater_max'),
        });
        $('#keyboard_NH4_N_7').jkeyboard({
            input: $('#gauge_NH4_N_min'),
        });
        $('#keyboard_NH4_N_8').jkeyboard({
            input: $('#gauge_NH4_N_max'),
        });
        // ============================================================
        // ============================================================
        // ============================================================
        // ============================================================
    });
</script>
