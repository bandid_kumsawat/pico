
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config gauge_Salinity <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title'class='pico-setting'>
                Set Param Salinity
            </div>
        </center>
    </div>
</div>


<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Salinity [LOLO] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "alam_Salinity_lolo" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Salinity_lolo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Salinity_lolo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_Salinity_lolo'>
            </label>
        </div>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Salinity [LO] : </b>
        </div>
    </div>

    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "alam_Salinity_lo" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Salinity_lo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Salinity_lo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>

    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_Salinity_lo'>
            </label>
        </div>
    </div>
</div>
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Salinity [HI] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "alam_Salinity_hi" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Salinity_hi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Salinity_hi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_Salinity_hi'>
            </label>
        </div>
    </div>
</div>


<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Salinity [HIHI] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "alam_Salinity_hihi" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Salinity_hihi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Salinity_hihi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_Salinity_hihi'>
            </label>
        </div>
    </div>
</div>
<!-- -------------------------------------------------------- -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div  class='pico-setting'>
                Set Adjust Salinity Calculater
            </div>
        </center>
    </div>
</div>
<!-- <div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>value conduce: </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  value =  "800" type = "number" class="form-control" disabled id = "Adjust_Salinity_calculater" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('Adjust_Salinity_calculater')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('Adjust_Salinity_calculater')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div> -->
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>value salfac: </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  value =  "4000" type = "number" class="form-control" disabled id = "Adjust_Salinity_calculater_max" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('Adjust_Salinity_calculater_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('Adjust_Salinity_calculater_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>
<hr>
<!-- -------------------------------------------------------- -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title' class='pico-setting'>
                Set Salinity Gauge
            </div>
        </center>
    </div>
</div>

<div class='row'>
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Name  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" id = "gauge_Salinity_name" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-Salinity-gauge-name"/>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>min : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "gauge_Salinity_min" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('gauge_Salinity_min')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_Salinity_min')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>max : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "gauge_Salinity_max" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('gauge_Salinity_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_Salinity_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>

<div class='row' style = "margin-top:20px;" >
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>unit  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" value = " " type = "text" class="form-control" id = "gauge_Salinity_unit" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-Salinity-gauge-unit"/>
    </div>
</div>
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config gauge_Salinity <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config gauge_Salinity <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->


<script type="text/javascript"> 
    $( document ).ready(function() {
        $('#status_conf_alam_Salinity_lo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_Salinity_lolo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_Salinity_hi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_Salinity_hihi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#keyboard_Salinity_gauge_name').jkeyboard({
            // layout: "english",
            input: $('#gauge_Salinity_name'),
        });
        $('#keyboard_Salinity_gauge_unit').jkeyboard({
            // layout: "english",
            input: $('#gauge_Salinity_unit'),
        });
    });
</script>