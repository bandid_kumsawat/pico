
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config gauge_Line <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title'class='pico-setting'>
                Set Line Notify
            </div>
        </center>
    </div>
</div>

<div class='row'>
    <div class='col-md-6'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Token  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" id = "Line_token" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-line-token"/>
    </div>
</div>

 <!-- status lolo -->
<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-6'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Line Message Water quality [LoLo] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "line_quality_wt_lolo" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('line_quality_wt_lolo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('line_quality_wt_lolo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <div class = 'col-md-2'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_line_lolo'>
            </label>
        </div>
    </div>
</div>
<br>
<div hidden class='row'>
    <div class='col-md-6'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Message  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" id = "Line_lolo_mess" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-line-lolo-message"/>
    </div>
</div>
 <!-- end -->

  <!-- status lo -->
<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-6'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Line Message Water quality [lo] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "line_quality_wt_lo" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('line_quality_wt_lo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('line_quality_wt_lo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <div class = 'col-md-2'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_line_lo'>
            </label>
        </div>
    </div>
</div>
<br>
<div hidden class='row'>
    <div class='col-md-6'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Message  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" id = "Line_lo_mess" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-line-lo-message"/>
    </div>
</div>
 <!-- end -->

  <!-- status hi -->
  <div  hidden class='row' style = "margin-top:20px;">
    <div class='col-md-6'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Line Message Water quality [hi] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "line_quality_wt_hi" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('line_quality_wt_hi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('line_quality_wt_hi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <div class = 'col-md-2'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_line_hi'>
            </label>
        </div>
    </div>
</div>
<div  hidden class='row'>
    <div class='col-md-6'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Message  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" id = "Line_hi_mess" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-line-hi-message"/>
    </div>
</div>
 <!-- end -->

  <!-- status hihi -->
  <div  hidden class='row' style = "margin-top:20px;">
    <div class='col-md-6'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Line Message Water quality [hihi] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "line_quality_wt_hihi" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('line_quality_wt_hihi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('line_quality_wt_hihi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <div class = 'col-md-2'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_line_hihi'>
            </label>
        </div>
    </div>
</div>
<div  hidden class='row'>
    <div class='col-md-6'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Message  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" id = "Line_hihi_mess" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-line-hihi-message"/>
    </div>
</div>
 <!-- end -->

<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config gauge_Line <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->

<script type="text/javascript"> 
    $( document ).ready(function() {
        $('#status_conf_alam_line_lolo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_line_lo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_line_hi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_line_hihi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#keyboard_line_token').jkeyboard({
            // layout: "english",
            input: $('#Line_token'),
        });
        $('#keyboard_line_lolo_message').jkeyboard({
            // layout: "english",
            input: $('#Line_lolo_mess'),
        });
        $('#keyboard_line_lo_message').jkeyboard({
            // layout: "english",
            input: $('#Line_lo_mess'),
        });
        $('#keyboard_line_hi_message').jkeyboard({
            // layout: "english",
            input: $('#Line_hi_mess'),
        });
        $('#keyboard_line_hihi_message').jkeyboard({
            // layout: "english",
            input: $('#Line_hihi_mess'),
        });
    });
</script>
