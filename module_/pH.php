
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config gauge_ph <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title'class='pico-setting'>
                Set Param pH
            </div>
        </center>
    </div>
</div>


<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm pH [LOLO] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "alam_pH_lolo" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_pH_lolo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_pH_lolo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_pH_lolo'>
            </label>
        </div>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm pH [LO] : </b>
        </div>
    </div>

    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "alam_pH_lo" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_pH_lo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_pH_lo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>

    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_pH_lo'>
            </label>
        </div>
    </div>
</div>
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm pH [HI] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "alam_pH_hi" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_pH_hi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_pH_hi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_pH_hi'>
            </label>
        </div>
    </div>
</div>


<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm pH [HIHI] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "alam_pH_hihi" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_pH_hihi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_pH_hihi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input checked type='checkbox'  value = '1' id = 'status_conf_alam_pH_hihi'>
            </label>
        </div>
    </div>
</div>
<!-- -------------------------------------------------------- -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div  class='pico-setting'>
                Set Adjust pH Calculater
            </div>
        </center>
    </div>
</div>
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>value min: </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  value =  "800" type = "number" class="form-control" disabled id = "Adjust_pH_calculater" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('Adjust_pH_calculater')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('Adjust_pH_calculater')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>value max: </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  value =  "4000" type = "number" class="form-control" disabled id = "Adjust_pH_calculater_max" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('Adjust_pH_calculater_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('Adjust_pH_calculater_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>
<hr>
<!-- -------------------------------------------------------- -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title' class='pico-setting'>
                Set pH Gauge
            </div>
        </center>
    </div>
</div>

<div class='row'>
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Name  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" id = "gauge_ph_name" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-ph-gauge-name"/>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>min : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "gauge_ph_min" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('gauge_ph_min')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_ph_min')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>max : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "number" class="form-control" disabled id = "gauge_ph_max" style = "padding: 12px;text-align: center;font-size: 20px;" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('gauge_ph_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_ph_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>

<div class='row' style = "margin-top:20px;" >
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>unit  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" value = " " type = "text" class="form-control" id = "gauge_ph_unit" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-ph-gauge-unit"/>
    </div>
</div>
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config gauge_ph <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->


<script type="text/javascript"> 
    $( document ).ready(function() {
        $('#status_conf_alam_pH_lo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_pH_lolo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_pH_hi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_pH_hihi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#keyboard_ph_gauge_name').jkeyboard({
            // layout: "english",
            input: $('#gauge_ph_name'),
        });
        $('#keyboard_ph_gauge_unit').jkeyboard({
            // layout: "english",
            input: $('#gauge_ph_unit'),
        });
    });
</script>