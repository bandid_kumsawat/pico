var pico_config = global.get("pico_config");
var alarm_status = [];
var count_ = 0;

alarm_status[count_] = {
    "pH":{
        "vstatus": pico_config[0].pH_param.gauge_ph.min
    }
}
count_++;
if (pico_config[0].pH_param.status_conf_alam_pH_lolo){
    alarm_status[count_] = {
        "pH":{
            "value": -2,
            "vstatus": pico_config[0].pH_param.alam_pH_lolo
        }
    }
    count_++;
}
if (pico_config[0].pH_param.status_conf_alam_pH_lo){
    alarm_status[count_] = {
        "pH":{
            "value": -1,
            "vstatus": pico_config[0].pH_param.alam_pH_lo
        }
    }
    count_++;
}
if (pico_config[0].pH_param.status_conf_alam_pH_hi){
    alarm_status[count_] = {
        "pH":{
            "value": 0,
            "vstatus": pico_config[0].pH_param.alam_pH_hi
        }
    }
    count_++;
}
if (pico_config[0].pH_param.status_conf_alam_pH_hihi){
    alarm_status[count_] = {
        "pH":{
            "value": 1,
            "vstatus": pico_config[0].pH_param.alam_pH_hihi
        }
    }
    count_++;
}
alarm_status[count_] = {
    "pH":{
        "value": 2,
        "vstatus": pico_config[0].pH_param.gauge_ph.max
    }
}

count_ = 0;

function ph_status(alarm_status_fc,data_ph){
    for (var i = 0;i< (alarm_status_fc.length - 1);i++){
        if (i === 0){
            if (data_ph >= (parseFloat(alarm_status_fc[i].pH.vstatus)) && (data_ph < (parseFloat(alarm_status_fc[i + 1].pH.vstatus)))){
                return alarm_status_fc[i + 1].pH.value
            }
        }else if (i === (alarm_status_fc.length - 1)){
            if (data_ph > (parseFloat(alarm_status_fc[i].pH.vstatus)) && (data_ph <= (parseFloat(alarm_status_fc[i + 1].pH.vstatus)))){
                return alarm_status_fc[i + 1].pH.value
            }
        }else{
            if (data_ph >= (parseFloat(alarm_status_fc[i].pH.vstatus)) && (data_ph <= (parseFloat(alarm_status_fc[i + 1].pH.vstatus)))){
                return alarm_status_fc[i + 1].pH.value
            }
        }
    }
}

// for (var i = 0;i <= 14;i++){
//     msg.payload[i] = ph_status(alarm_status,i);
// }
msg.payload = alarm_status;
return msg

// DISPLAY=:0 /usr/bin/chromium-browser  --enable-native-gpu-memory-buffers --force-device-scale-factor=0.9  --noerrdialogs --incognito --disable-infobars --kiosk http://127.0.0.1/dashboard.php &