<!doctype html>
<html lang="en">
<head>
    <title>PICO</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bsadmin.css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="keypad/jkeyboard-master/lib/css/jkeyboard.css">
    <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.min.css" />
</head>
<style>
    body{
        font-family: "Noto Sans Thai", 'Arimo', ystem-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Fira Sans", "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 1.5rem;
        color: #212121;
    }
    .pico-content{
        width: 1090px;
        height: 100%;
        background-color:#ffffff;
        position: relative;
        top: 0px;
        left: 10px;
        z-index: 1;
    }
    .pico-setting{
        font-size: 40px;
    }
    .pico-node-red-status{
        position: fixed;
        font-size:30px;
        right: 150px;
        top: 10px;
        z-index:3;
        color:rgb(47, 116, 8);
    }
    #keyboard{
        position: fixed;
        background: #747373;
        padding: 10px;
        border-radius: 6px;
    }
    .modal-dialog {
        position:fixed;
        top:300px;
        right:auto;
        left:100px;
        bottom:0px;
    }
    .modal-lg {
        width: 820px;
        margin: auto;
    }
    .modal-backdrop
    {
        opacity:0 !important;
    }

</style>
<body style="background-color:rgb(255, 255, 255)">

<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bsadmin.js"></script>
<script type="text/javascript" src="button-toggle/js/bootstrap2-toggle.min.js"></script>
<script src="keypad/jkeyboard-master/lib/js/jkeyboard.js"></script>

<nav class="navbar navbar-expand navbar-dark bg-dark" style = "font-size: 30px;">
    <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>
    <a class="navbar-brand" href="#"></a>
    <!-- <div class='pico-node-red-status'><i class="fa fa-circle"> <p class='pico-text-status'>Active  </p></i></div> -->
</nav>

<div class="d-flex" style = "height:1000px;">
    <nav class="sidebar bg-dark toggled">
        <ul class="list-unstyled">
            <li><a href="dashboard.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
            <li><a href="tables.php"><i class="fa fa-fw fa-table"></i> Record</a></li>
            <li><a href="control.php"><i class="fa fa-fw fa-hand-o-up"></i> Control</a></li>
            <li><a href="settings.php"><i class="fa fa-fw fa-gears"></i> Setting</a></li>
            <li><a href="login/index_security.php"><i class="fa fa-fw fa-lock"></i> Security</a></li>
            <li><a href="safemode.php"><i class="fa fa-fw fa-hand-o-up"></i> Safe Mode</a></li>
        </ul>
        </ul>
    </nav>
    <div class='pico-content'>
        <div class='content p-4'>

            <div class='row p-4'>
                <div class='col-sm-12'>
                    <center>
                        <div id = 'title'class='pico-setting'>
                            Setting
                        </div>
                    </center>
                </div>
            </div>




            <div class='row'>
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Set mode  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class='checkbox'>
                        <label>
                            <input type='checkbox'  value = '1' id = 'set_mode'>
                        </label>
                    </div>
                </div>
            </div>

            <br><br>

            <div class='row'>
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Title  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" data-toggle="modal" data-target=".bd-example-modal-lg-title" id = "title_" style = "padding: 12px;"/>
                </div>
            </div>

            <br>
            
            <!-- add station id config here -->
            <div class='row'>
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Station ID  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" data-toggle="modal" data-target=".bd-example-modal-lg-status-id" id = "station_id" style = "padding: 12px;"/>
                </div>
            </div>




            <hr>
            <div class='row p-4'>
                <div class='col-sm-12'>
                    <center>
                        <div id = 'title'class='pico-setting'>
                            Time
                        </div>
                    </center>
                </div>
            </div>


            <!-- for purge  -->
            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Purge run : </b>
                    </div>
                </div>
                <div class='col-md-2'>
                    <input  type = "number" value = '30' class="form-control"  disabled id = "purge_on" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('purge_on')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('purge_on')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
                <div class='col-md-2'>
                    min
                </div>
            </div>
            <!-- for purge  -->


            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Pump run : </b>
                    </div>
                </div>
                <div class='col-md-2'>
                    <input  type = "number" value = '60' class="form-control"  disabled id = "pump_on" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('pump_on')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('pump_on')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
                <div class='col-md-2'>
                    min
                </div>
            </div>


            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Pump stop : </b>
                    </div>
                </div>
                <div class='col-md-2'>
                    <input  type = "number" class="form-control"  disabled id = "pump_off" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('pump_off')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('pump_off')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
                <div class='col-md-2'>
                    min
                </div>
            </div>

            <div hidden class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Pump Run : </b>
                    </div>
                </div>
                <div class='col-md-2'>
                    <input  type = "number" class="form-control"  disabled id = "pump_run_" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('pump_run_')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('pump_run_')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
                <div class='col-md-2'>
                    min
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Delay : </b>
                    </div>
                </div>
                <div class='col-md-2'>
                    <input  type = "number" class="form-control"  disabled id = "delay_" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('delay_')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('delay_')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
                <div class='col-md-2'>
                    min
                </div>
            </div>
            
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config pH <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
                <?php  include("./module/pH.php");  ?>
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config pH <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config DO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <?php  include("./module/DO.php");  ?>
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config DO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config EC <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <?php  include("./module/EC.php");  ?>
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config EC <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config tur <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <?php  include("./module/Turbidity.php");  ?>
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config tur <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config nh4 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <?php  include("./module/NH4_N.php");  ?>
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config nh4 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config tds  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <?php  include("./module/TDS.php");  ?>
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config tds <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config temp  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <?php  include("./module/Temperature.php");  ?>
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config temp  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->

            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config Salinity <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <?php  include("./module/Salinity.php");  ?>
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config Salinity <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->

            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config Line <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
            <?php  include("./module/line_notify.php");  ?>
            <!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config Line <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->

            <!-- show hide gauge all -->
            <hr>

            <div class='row p-4'>
                <div class='col-sm-12'>
                    <center>
                        <div id = 'title' class='pico-setting'>
                            Display parmeter on dashboard
                        </div>
                    </center>
                </div>
            </div>
            <!-- ph -->
            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>pH  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class = 'col-md-4'>
                        <div class='checkbox'>
                            <label>
                                <input type='checkbox'  value = '1' id = 'pH_show_hide'>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->

            <!-- DO -->
            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>DO  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class = 'col-md-4'>
                        <div class='checkbox'>
                            <label>
                                <input checked type='checkbox'  value = '1' id = 'DO_show_hide'>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->

            <!-- EC -->
            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>EC  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class = 'col-md-4'>
                        <div class='checkbox'>
                            <label>
                                <input checked type='checkbox'  value = '1' id = 'EC_show_hide'>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->

            <!-- Turbidity -->
            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Turbidity  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class = 'col-md-4'>
                        <div class='checkbox'>
                            <label>
                                <input checked type='checkbox'  value = '1' id = 'Turbidity_show_hide'>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->

            <!-- NH4_N -->
            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>NH4_N  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class = 'col-md-4'>
                        <div class='checkbox'>
                            <label>
                                <input checked type='checkbox'  value = '1' id = 'NH4_N_show_hide'>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->
            <!-- TDS -->
            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>TDS  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class = 'col-md-4'>
                        <div class='checkbox'>
                            <label>
                                <input checked type='checkbox'  value = '1' id = 'TDS_show_hide'>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->
            <!-- Temperature -->
            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Temperature  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class = 'col-md-4'>
                        <div class='checkbox'>
                            <label>
                                <input checked type='checkbox'  value = '1' id = 'Temperature_show_hide'>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->
            <!-- Spare -->
            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Spare  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class = 'col-md-4'>
                        <div class='checkbox'>
                            <label>
                                <input checked type='checkbox'  value = '1' id = 'Spare_show_hide'>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->
            <!-- Salinity -->
            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Salinity  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class = 'col-md-4'>
                        <div class='checkbox'>
                            <label>
                                <input checked type='checkbox'  value = '1' id = 'Salinity_show_hide'>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->
            <hr>
            <div class='row' style = "margin-top:20px;">
                <div class='col-md-3'></div>
                <div class='col-md-3'></div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "onclick_()"style = "width: 100%;font-size:24px;"> Save </button>
                </div>
            </div>

            <hr>
            <!-- export csv -->
            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>Export .csv</div>
                <div class='col-md-4'>
                    <button type = 'button' class="btn btn-primary" onclick = "export__()"style = "width: 100%;font-size:24px;"> Export </button>
                </div>
            </div>
            <!-- end -->
        </div>
    </div>
</div>


<?php
    include("./module/modal/content.php")
?>


</body>
<script type="text/javascript"> 
let tranfer_data = new WebSocket("ws://localhost:1880/wt/config");
let this_onload = new WebSocket("ws://localhost:1880/wt/onload");
let this_usbexport = new WebSocket("ws://localhost:1880/wt/usbexport");
let pagelogin = new WebSocket("ws://localhost:1880/wt/pagelogin");

$( document ).ready(function() {
    $('#set_mode').bootstrapToggle({on: 'Auto',off: 'Manaul'});
    setCookie("page", "settings.php", 1);
    if (getCookie("username") == ""){
        location.replace("login/index.php");
    }
    page_login();
    
    sock_to_update();
    function_usb_export();
    $('#keyboard_title').jkeyboard({
        // layout: "english",
        input: $('#title_'),
    });

    $('#keyboard_station_id').jkeyboard({
        // layout: "english",
        input: $('#station_id'),
    });


    onload_data();
    this_onload.send('{"cmd":"onload"}');
    document.body.style.zoom = "90%";
});
function sock_to_update(){
    tranfer_data.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    tranfer_data.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.result){
            alert("บันทึกข้อมูลเรียบร้อยแล้ว")
        }
    };

    tranfer_data.onclose = function(event) {
        if (event.wasClean) {
            // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
            location.reload();
        } else {
            // alert('[close] Connection died');
            location.reload();
        }
    };

    tranfer_data.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}
function page_login(){
    pagelogin.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    pagelogin.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.page_login){
            location.replace("login/index.php");
        }
    };

    pagelogin.onclose = function(event) {
        if (event.wasClean) {
            // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
            location.reload();
        } else {
            // alert('[close] Connection died');
            location.reload();
        }
    };

    pagelogin.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}
function function_usb_export(){
    this_usbexport.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    this_usbexport.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.usb_tranf.code == 0){
            alert("Export .csv [ OK ]")
        }else{
            alert("Export Error")
        }
    };

    this_usbexport.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    this_usbexport.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}

function onload_data(){
    this_onload.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
        request_data();
    };

    this_onload.onmessage = function(event) {
        var obj = JSON.parse(event.data);
        console.log(obj);
        $("#title_").val(obj[0].title)
        $("#station_id").val(obj[0].station_id)
        $("#pump_on").val(obj[0].pump_on)
        $("#purge_on").val(obj[0].purge_on)
        $("#pump_off").val(obj[0].pump_off)
        $("#delay_").val(obj[0].delay)
        // $("#set_mode").val(obj[0].set_mode)
        if (obj[0].set_mode){
            $('#set_mode').bootstrapToggle('on')
        }else{
            $('#set_mode').bootstrapToggle('off')
        }
        //  ph ---------------------------------------------------------
        $("#alam_pH_lo").val(obj[0].pH_param.alam_pH_lo)
        $("#alam_pH_lolo").val(obj[0].pH_param.alam_pH_lolo)
        $("#alam_pH_hi").val(obj[0].pH_param.alam_pH_hi)
        $("#alam_pH_hihi").val(obj[0].pH_param.alam_pH_hihi)
        $("#Adjust_pH_calculater").val(obj[0].pH_param.Adjust_pH_calculater)
        $("#Adjust_pH_calculater_max").val(obj[0].pH_param.Adjust_pH_calculater_max)
            // gauge
            $("#gauge_ph_name").val(obj[0].pH_param.gauge_ph.name)
            $("#gauge_ph_min").val(obj[0].pH_param.gauge_ph.min)
            $("#gauge_ph_max").val(obj[0].pH_param.gauge_ph.max)
            $("#gauge_ph_unit").val(obj[0].pH_param.gauge_ph.unit)
            // end gauge
        if (obj[0].pH_param.status_conf_alam_pH_lo){
            $('#status_conf_alam_pH_lo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_pH_lo').bootstrapToggle('off')
        }
        if (obj[0].pH_param.status_conf_alam_pH_lolo){
            $('#status_conf_alam_pH_lolo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_pH_lolo').bootstrapToggle('off')
        }
        if (obj[0].pH_param.status_conf_alam_pH_hi){
            $('#status_conf_alam_pH_hi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_pH_hi').bootstrapToggle('off')
        }
        if (obj[0].pH_param.status_conf_alam_pH_hihi){
            $('#status_conf_alam_pH_hihi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_pH_hihi').bootstrapToggle('off')
        }
        // end  ph ------------------------------------------------------

        //  DO ---------------------------------------------------------
        $("#alam_DO_lo").val(obj[0].DO_param.alam_DO_lo)
        $("#alam_DO_lolo").val(obj[0].DO_param.alam_DO_lolo)
        $("#alam_DO_hi").val(obj[0].DO_param.alam_DO_hi)
        $("#alam_DO_hihi").val(obj[0].DO_param.alam_DO_hihi)
        $("#Adjust_DO_calculater").val(obj[0].DO_param.Adjust_DO_calculater)
        $("#Adjust_DO_calculater_max").val(obj[0].DO_param.Adjust_DO_calculater_max)
            // gauge
            $("#gauge_DO_name").val(obj[0].DO_param.gauge_DO.name)
            $("#gauge_DO_min").val(obj[0].DO_param.gauge_DO.min)
            $("#gauge_DO_max").val(obj[0].DO_param.gauge_DO.max)
            $("#gauge_DO_unit").val(obj[0].DO_param.gauge_DO.unit)
            // end gauge
        if (obj[0].DO_param.status_conf_alam_DO_lo){
            $('#status_conf_alam_DO_lo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_DO_lo').bootstrapToggle('off')
        }
        if (obj[0].DO_param.status_conf_alam_DO_lolo){
            $('#status_conf_alam_DO_lolo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_DO_lolo').bootstrapToggle('off')
        }
        if (obj[0].DO_param.status_conf_alam_DO_hi){
            $('#status_conf_alam_DO_hi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_DO_hi').bootstrapToggle('off')
        }
        if (obj[0].DO_param.status_conf_alam_DO_hihi){
            $('#status_conf_alam_DO_hihi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_DO_hihi').bootstrapToggle('off')
        }
        // end  DO ------------------------------------------------------

        //  EC ---------------------------------------------------------
        $("#alam_EC_lo").val(obj[0].EC_param.alam_EC_lo)
        $("#alam_EC_lolo").val(obj[0].EC_param.alam_EC_lolo)
        $("#alam_EC_hi").val(obj[0].EC_param.alam_EC_hi)
        $("#alam_EC_hihi").val(obj[0].EC_param.alam_EC_hihi)
        $("#Adjust_EC_calculater").val(obj[0].EC_param.Adjust_EC_calculater)
        $("#Adjust_EC_calculater_max").val(obj[0].EC_param.Adjust_EC_calculater_max)
            // gauge
            $("#gauge_EC_name").val(obj[0].EC_param.gauge_EC.name)
            $("#gauge_EC_min").val(obj[0].EC_param.gauge_EC.min)
            $("#gauge_EC_max").val(obj[0].EC_param.gauge_EC.max)
            $("#gauge_EC_unit").val(obj[0].EC_param.gauge_EC.unit)
            // end gauge
        if (obj[0].EC_param.status_conf_alam_EC_lo){
            $('#status_conf_alam_EC_lo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_EC_lo').bootstrapToggle('off')
        }
        if (obj[0].EC_param.status_conf_alam_EC_lolo){
            $('#status_conf_alam_EC_lolo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_EC_lolo').bootstrapToggle('off')
        }
        if (obj[0].EC_param.status_conf_alam_EC_hi){
            $('#status_conf_alam_EC_hi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_EC_hi').bootstrapToggle('off')
        }
        if (obj[0].EC_param.status_conf_alam_EC_hihi){
            $('#status_conf_alam_EC_hihi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_EC_hihi').bootstrapToggle('off')
        }
        // end  EC ------------------------------------------------------

        //  Turbidity ---------------------------------------------------------
        $("#alam_Turbidity_lo").val(obj[0].Turbidity_param.alam_Turbidity_lo)
        $("#alam_Turbidity_lolo").val(obj[0].Turbidity_param.alam_Turbidity_lolo)
        $("#alam_Turbidity_hi").val(obj[0].Turbidity_param.alam_Turbidity_hi)
        $("#alam_Turbidity_hihi").val(obj[0].Turbidity_param.alam_Turbidity_hihi)
        $("#Adjust_Turbidity_calculater").val(obj[0].Turbidity_param.Adjust_Turbidity_calculater)
        $("#Adjust_Turbidity_calculater_max").val(obj[0].Turbidity_param.Adjust_Turbidity_calculater_max)
            // gauge
            $("#gauge_Turbidity_name").val(obj[0].Turbidity_param.gauge_Turbidity.name)
            $("#gauge_Turbidity_min").val(obj[0].Turbidity_param.gauge_Turbidity.min)
            $("#gauge_Turbidity_max").val(obj[0].Turbidity_param.gauge_Turbidity.max)
            $("#gauge_Turbidity_unit").val(obj[0].Turbidity_param.gauge_Turbidity.unit)
            // end gauge
        if (obj[0].Turbidity_param.status_conf_alam_Turbidity_lo){
            $('#status_conf_alam_Turbidity_lo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Turbidity_lo').bootstrapToggle('off')
        }
        if (obj[0].Turbidity_param.status_conf_alam_Turbidity_lolo){
            $('#status_conf_alam_Turbidity_lolo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Turbidity_lolo').bootstrapToggle('off')
        }
        if (obj[0].Turbidity_param.status_conf_alam_Turbidity_hi){
            $('#status_conf_alam_Turbidity_hi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Turbidity_hi').bootstrapToggle('off')
        }
        if (obj[0].Turbidity_param.status_conf_alam_Turbidity_hihi){
            $('#status_conf_alam_Turbidity_hihi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Turbidity_hihi').bootstrapToggle('off')
        }
        // end  Turbidity ------------------------------------------------------

        //  NH4_N ---------------------------------------------------------
        $("#alam_NH4_N_lo").val(obj[0].NH4_N_param.alam_NH4_N_lo)
        $("#alam_NH4_N_lolo").val(obj[0].NH4_N_param.alam_NH4_N_lolo)
        $("#alam_NH4_N_hi").val(obj[0].NH4_N_param.alam_NH4_N_hi)
        $("#alam_NH4_N_hihi").val(obj[0].NH4_N_param.alam_NH4_N_hihi)
        $("#Adjust_NH4_N_calculater").val(obj[0].NH4_N_param.Adjust_NH4_N_calculater)
        $("#Adjust_NH4_N_calculater_max").val(obj[0].NH4_N_param.Adjust_NH4_N_calculater_max)
            // gauge
            $("#gauge_NH4_N_name").val(obj[0].NH4_N_param.gauge_NH4_N.name)
            $("#gauge_NH4_N_min").val(obj[0].NH4_N_param.gauge_NH4_N.min)
            $("#gauge_NH4_N_max").val(obj[0].NH4_N_param.gauge_NH4_N.max)
            $("#gauge_NH4_N_unit").val(obj[0].NH4_N_param.gauge_NH4_N.unit)
            // end gauge
        if (obj[0].NH4_N_param.status_conf_alam_NH4_N_lo){
            $('#status_conf_alam_NH4_N_lo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_NH4_N_lo').bootstrapToggle('off')
        }
        if (obj[0].NH4_N_param.status_conf_alam_NH4_N_lolo){
            $('#status_conf_alam_NH4_N_lolo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_NH4_N_lolo').bootstrapToggle('off')
        }
        if (obj[0].NH4_N_param.status_conf_alam_NH4_N_hi){
            $('#status_conf_alam_NH4_N_hi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_NH4_N_hi').bootstrapToggle('off')
        }
        if (obj[0].NH4_N_param.status_conf_alam_NH4_N_hihi){
            $('#status_conf_alam_NH4_N_hihi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_NH4_N_hihi').bootstrapToggle('off')
        }
        // end  NH4_N ------------------------------------------------------
        //  TDS ---------------------------------------------------------
        $("#alam_TDS_lo").val(obj[0].TDS_param.alam_TDS_lo)
        $("#alam_TDS_lolo").val(obj[0].TDS_param.alam_TDS_lolo)
        $("#alam_TDS_hi").val(obj[0].TDS_param.alam_TDS_hi)
        $("#alam_TDS_hihi").val(obj[0].TDS_param.alam_TDS_hihi)
        $("#Adjust_TDS_calculater").val(obj[0].TDS_param.Adjust_TDS_calculater)
        $("#Adjust_TDS_calculater_max").val(obj[0].TDS_param.Adjust_TDS_calculater_max)
            // gauge
            $("#gauge_TDS_name").val(obj[0].TDS_param.gauge_TDS.name)
            $("#gauge_TDS_min").val(obj[0].TDS_param.gauge_TDS.min)
            $("#gauge_TDS_max").val(obj[0].TDS_param.gauge_TDS.max)
            $("#gauge_TDS_unit").val(obj[0].TDS_param.gauge_TDS.unit)
            // end gauge
        if (obj[0].TDS_param.status_conf_alam_TDS_lo){
            $('#status_conf_alam_TDS_lo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_TDS_lo').bootstrapToggle('off')
        }
        if (obj[0].TDS_param.status_conf_alam_TDS_lolo){
            $('#status_conf_alam_TDS_lolo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_TDS_lolo').bootstrapToggle('off')
        }
        if (obj[0].TDS_param.status_conf_alam_TDS_hi){
            $('#status_conf_alam_TDS_hi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_TDS_hi').bootstrapToggle('off')
        }
        if (obj[0].TDS_param.status_conf_alam_TDS_hihi){
            $('#status_conf_alam_TDS_hihi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_TDS_hihi').bootstrapToggle('off')
        }
        // end  TDS ------------------------------------------------------
        //  Temperature ---------------------------------------------------------
        $("#alam_Temperature_lo").val(obj[0].Temperature_param.alam_Temperature_lo)
        $("#alam_Temperature_lolo").val(obj[0].Temperature_param.alam_Temperature_lolo)
        $("#alam_Temperature_hi").val(obj[0].Temperature_param.alam_Temperature_hi)
        $("#alam_Temperature_hihi").val(obj[0].Temperature_param.alam_Temperature_hihi)
        $("#Adjust_Temperature_calculater").val(obj[0].Temperature_param.Adjust_Temperature_calculater)
        $("#Adjust_Temperature_calculater_max").val(obj[0].Temperature_param.Adjust_Temperature_calculater_max)
            // gauge
            $("#gauge_Temperature_name").val(obj[0].Temperature_param.gauge_Temperature.name)
            $("#gauge_Temperature_min").val(obj[0].Temperature_param.gauge_Temperature.min)
            $("#gauge_Temperature_max").val(obj[0].Temperature_param.gauge_Temperature.max)
            $("#gauge_Temperature_unit").val(obj[0].Temperature_param.gauge_Temperature.unit)
            // end gauge
        if (obj[0].Temperature_param.status_conf_alam_Temperature_lo){
            $('#status_conf_alam_Temperature_lo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Temperature_lo').bootstrapToggle('off')
        }
        if (obj[0].Temperature_param.status_conf_alam_Temperature_lolo){
            $('#status_conf_alam_Temperature_lolo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Temperature_lolo').bootstrapToggle('off')
        }
        if (obj[0].Temperature_param.status_conf_alam_Temperature_hi){
            $('#status_conf_alam_Temperature_hi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Temperature_hi').bootstrapToggle('off')
        }
        if (obj[0].Temperature_param.status_conf_alam_Temperature_hihi){
            $('#status_conf_alam_Temperature_hihi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Temperature_hihi').bootstrapToggle('off')
        }
        // end  Temperature ------------------------------------------------------
        //  Salinity ---------------------------------------------------------
        $("#alam_Salinity_lo").val(obj[0].Salinity_param.alam_Salinity_lo)
        $("#alam_Salinity_lolo").val(obj[0].Salinity_param.alam_Salinity_lolo)
        $("#alam_Salinity_hi").val(obj[0].Salinity_param.alam_Salinity_hi)
        $("#alam_Salinity_hihi").val(obj[0].Salinity_param.alam_Salinity_hihi)
        $("#Adjust_Salinity_calculater").val(obj[0].Salinity_param.Adjust_Salinity_calculater)
        $("#Adjust_Salinity_calculater_max").val(obj[0].Salinity_param.Adjust_Salinity_calculater_max)
            // gauge
            $("#gauge_Salinity_name").val(obj[0].Salinity_param.gauge_Salinity.name)
            $("#gauge_Salinity_min").val(obj[0].Salinity_param.gauge_Salinity.min)
            $("#gauge_Salinity_max").val(obj[0].Salinity_param.gauge_Salinity.max)
            $("#gauge_Salinity_unit").val(obj[0].Salinity_param.gauge_Salinity.unit)
            // end gauge
        if (obj[0].Salinity_param.status_conf_alam_Salinity_lo){
            $('#status_conf_alam_Salinity_lo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Salinity_lo').bootstrapToggle('off')
        }
        if (obj[0].Salinity_param.status_conf_alam_Salinity_lolo){
            $('#status_conf_alam_Salinity_lolo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Salinity_lolo').bootstrapToggle('off')
        }
        if (obj[0].Salinity_param.status_conf_alam_Salinity_hi){
            $('#status_conf_alam_Salinity_hi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Salinity_hi').bootstrapToggle('off')
        }
        if (obj[0].Salinity_param.status_conf_alam_Salinity_hihi){
            $('#status_conf_alam_Salinity_hihi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_Salinity_hihi').bootstrapToggle('off')
        }
        // end  Salinity ------------------------------------------------------
        //  Line ---------------------------------------------------------
        $("#Line_token").val(obj[0].Line_param.Line_token)
        $("#line_quality_wt_lolo").val(obj[0].Line_param.line_quality_wt_lolo)
        $("#line_quality_wt_lo").val(obj[0].Line_param.line_quality_wt_lo)
        $("#line_quality_wt_hi").val(obj[0].Line_param.line_quality_wt_hi)
        $("#line_quality_wt_hihi").val(obj[0].Line_param.line_quality_wt_hihi)

        $("#Line_lolo_mess").val(obj[0].Line_param.Line_lolo_mess)
        $("#Line_lo_mess").val(obj[0].Line_param.Line_lo_mess)
        $("#Line_hi_mess").val(obj[0].Line_param.Line_hi_mess)
        $("#Line_hihi_mess").val(obj[0].Line_param.Line_hihi_mess)
        if (obj[0].Line_param.status_conf_alam_line_lolo){
            $('#status_conf_alam_line_lolo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_line_lolo').bootstrapToggle('off')
        }
        if (obj[0].Line_param.status_conf_alam_line_lo){
            $('#status_conf_alam_line_lo').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_line_lo').bootstrapToggle('off')
        }
        if (obj[0].Line_param.status_conf_alam_line_hi){
            $('#status_conf_alam_line_hi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_line_hi').bootstrapToggle('off')
        }
        if (obj[0].Line_param.status_conf_alam_line_hihi){
            $('#status_conf_alam_line_hihi').bootstrapToggle('on')
        }else{
            $('#status_conf_alam_line_hihi').bootstrapToggle('off')
        }

        //config show hide gauge
        $('#pH_show_hide').bootstrapToggle({on: 'Show',off: 'Hide'});
        $('#DO_show_hide').bootstrapToggle({on: 'Show',off: 'Hide'});
        $('#EC_show_hide').bootstrapToggle({on: 'Show',off: 'Hide'});
        $('#Turbidity_show_hide').bootstrapToggle({on: 'Show',off: 'Hide'});
        $('#NH4_N_show_hide').bootstrapToggle({on: 'Show',off: 'Hide'});
        $('#TDS_show_hide').bootstrapToggle({on: 'Show',off: 'Hide'});
        $('#Temperature_show_hide').bootstrapToggle({on: 'Show',off: 'Hide'});
        $('#Spare_show_hide').bootstrapToggle({on: 'Show',off: 'Hide'});
        $('#Salinity_show_hide').bootstrapToggle({on: 'Show',off: 'Hide'});

        if (obj[0].dashboard_config_show_hide.pH){
            $('#pH_show_hide').bootstrapToggle('on')
        }else{
            $('#pH_show_hide').bootstrapToggle('off')
        }

        if (obj[0].dashboard_config_show_hide.DO){
            $('#DO_show_hide').bootstrapToggle('on')
        }else{
            $('#DO_show_hide').bootstrapToggle('off')
        }

        if (obj[0].dashboard_config_show_hide.EC){
            $('#EC_show_hide').bootstrapToggle('on')
        }else{
            $('#EC_show_hide').bootstrapToggle('off')
        }

        if (obj[0].dashboard_config_show_hide.Turbidity){
            $('#Turbidity_show_hide').bootstrapToggle('on')
        }else{
            $('#Turbidity_show_hide').bootstrapToggle('off')
        }

        if (obj[0].dashboard_config_show_hide.NH4_N){
            $('#NH4_N_show_hide').bootstrapToggle('on')
        }else{
            $('#NH4_N_show_hide').bootstrapToggle('off')
        }

        if (obj[0].dashboard_config_show_hide.TDS){
            $('#TDS_show_hide').bootstrapToggle('on')
        }else{
            $('#TDS_show_hide').bootstrapToggle('off')
        }

        if (obj[0].dashboard_config_show_hide.Temperature){
            $('#Temperature_show_hide').bootstrapToggle('on')
        }else{
            $('#Temperature_show_hide').bootstrapToggle('off')
        }

        if (obj[0].dashboard_config_show_hide.Spare){
            $('#Spare_show_hide').bootstrapToggle('on')
        }else{
            $('#Spare_show_hide').bootstrapToggle('off')
        }
        if (obj[0].dashboard_config_show_hide.Salinity_show_hide){
            $('#Salinity_show_hide').bootstrapToggle('on')
        }else{
            $('#Salinity_show_hide').bootstrapToggle('off')
        }
    };

    this_onload.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    this_onload.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };

}


function request_data(){
    setTimeout(function() {
    }, 1000);
    this_onload.send('{"cmd":"onload"}');
}

function onclick_(){
    var json_obj = {
        "_id":1,
        "pump_on": $("#pump_on").val(),
        "purge_on": $("#purge_on").val(),
        "delay":$("#delay_").val(),
        "pump_off":$("#pump_off").val(),
        "title":$("#title_").val(),
        "station_id":$("#station_id").val(),
        "set_mode": $("#set_mode").prop('checked'),
        // ph --------------------------------------------------------------------------
        "pH_param":{
            "alam_pH_lo": $("#alam_pH_lo").val(),
            "status_conf_alam_pH_lo": $("#status_conf_alam_pH_lo").prop('checked'),
            "alam_pH_lolo": $("#alam_pH_lolo").val(),
            "status_conf_alam_pH_lolo": $("#status_conf_alam_pH_lolo").prop('checked'),
            "alam_pH_hi": $("#alam_pH_hi").val(),
            "status_conf_alam_pH_hi": $("#status_conf_alam_pH_hi").prop('checked'),
            "alam_pH_hihi": $("#alam_pH_hihi").val(),
            "status_conf_alam_pH_hihi": $("#status_conf_alam_pH_hihi").prop('checked'),
            "gauge_ph":{
                "name": $("#gauge_ph_name").val(),
                "min": $("#gauge_ph_min").val(),
                "max": $("#gauge_ph_max").val(),
                "unit": $("#gauge_ph_unit").val(),
            },
            "Adjust_pH_calculater": $("#Adjust_pH_calculater").val(),
            "Adjust_pH_calculater_max": $("#Adjust_pH_calculater_max").val()
        },
        // end -------------------------------------------------------------------------

        // DO --------------------------------------------------------------------------
        "DO_param":{
            "alam_DO_lo": $("#alam_DO_lo").val(),
            "status_conf_alam_DO_lo": $("#status_conf_alam_DO_lo").prop('checked'),
            "alam_DO_lolo": $("#alam_DO_lolo").val(),
            "status_conf_alam_DO_lolo": $("#status_conf_alam_DO_lolo").prop('checked'),
            "alam_DO_hi": $("#alam_DO_hi").val(),
            "status_conf_alam_DO_hi": $("#status_conf_alam_DO_hi").prop('checked'),
            "alam_DO_hihi": $("#alam_DO_hihi").val(),
            "status_conf_alam_DO_hihi": $("#status_conf_alam_DO_hihi").prop('checked'),
            "gauge_DO":{
                "name": $("#gauge_DO_name").val(),
                "min": $("#gauge_DO_min").val(),
                "max": $("#gauge_DO_max").val(),
                "unit": $("#gauge_DO_unit").val(),
            },
            "Adjust_DO_calculater": $("#Adjust_DO_calculater").val(),
            "Adjust_DO_calculater_max": $("#Adjust_DO_calculater_max").val()
        },
        // end -------------------------------------------------------------------------

        // EC --------------------------------------------------------------------------
        "EC_param":{
            "alam_EC_lo": $("#alam_EC_lo").val(),
            "status_conf_alam_EC_lo": $("#status_conf_alam_EC_lo").prop('checked'),
            "alam_EC_lolo": $("#alam_EC_lolo").val(),
            "status_conf_alam_EC_lolo": $("#status_conf_alam_EC_lolo").prop('checked'),
            "alam_EC_hi": $("#alam_EC_hi").val(),
            "status_conf_alam_EC_hi": $("#status_conf_alam_EC_hi").prop('checked'),
            "alam_EC_hihi": $("#alam_EC_hihi").val(),
            "status_conf_alam_EC_hihi": $("#status_conf_alam_EC_hihi").prop('checked'),
            "gauge_EC":{
                "name": $("#gauge_EC_name").val(),
                "min": $("#gauge_EC_min").val(),
                "max": $("#gauge_EC_max").val(),
                "unit": $("#gauge_EC_unit").val(),
            },
            "Adjust_EC_calculater": $("#Adjust_EC_calculater").val(),
            "Adjust_EC_calculater_max": $("#Adjust_EC_calculater_max").val()
        },
        // end -------------------------------------------------------------------------

        // Turbidity --------------------------------------------------------------------------
        "Turbidity_param":{
            "alam_Turbidity_lo": $("#alam_Turbidity_lo").val(),
            "status_conf_alam_Turbidity_lo": $("#status_conf_alam_Turbidity_lo").prop('checked'),
            "alam_Turbidity_lolo": $("#alam_Turbidity_lolo").val(),
            "status_conf_alam_Turbidity_lolo": $("#status_conf_alam_Turbidity_lolo").prop('checked'),
            "alam_Turbidity_hi": $("#alam_Turbidity_hi").val(),
            "status_conf_alam_Turbidity_hi": $("#status_conf_alam_Turbidity_hi").prop('checked'),
            "alam_Turbidity_hihi": $("#alam_Turbidity_hihi").val(),
            "status_conf_alam_Turbidity_hihi": $("#status_conf_alam_Turbidity_hihi").prop('checked'),
            "gauge_Turbidity":{
                "name": $("#gauge_Turbidity_name").val(),
                "min": $("#gauge_Turbidity_min").val(),
                "max": $("#gauge_Turbidity_max").val(),
                "unit": $("#gauge_Turbidity_unit").val(),
            },
            "Adjust_Turbidity_calculater": $("#Adjust_Turbidity_calculater").val(),
            "Adjust_Turbidity_calculater_max": $("#Adjust_Turbidity_calculater_max").val()
        },
        // end -------------------------------------------------------------------------

        // NH4_N --------------------------------------------------------------------------
        "NH4_N_param":{
            "alam_NH4_N_lo": $("#alam_NH4_N_lo").val(),
            "status_conf_alam_NH4_N_lo": $("#status_conf_alam_NH4_N_lo").prop('checked'),
            "alam_NH4_N_lolo": $("#alam_NH4_N_lolo").val(),
            "status_conf_alam_NH4_N_lolo": $("#status_conf_alam_NH4_N_lolo").prop('checked'),
            "alam_NH4_N_hi": $("#alam_NH4_N_hi").val(),
            "status_conf_alam_NH4_N_hi": $("#status_conf_alam_NH4_N_hi").prop('checked'),
            "alam_NH4_N_hihi": $("#alam_NH4_N_hihi").val(),
            "status_conf_alam_NH4_N_hihi": $("#status_conf_alam_NH4_N_hihi").prop('checked'),
            "gauge_NH4_N":{
                "name": $("#gauge_NH4_N_name").val(),
                "min": $("#gauge_NH4_N_min").val(),
                "max": $("#gauge_NH4_N_max").val(),
                "unit": $("#gauge_NH4_N_unit").val(),
            },
            "Adjust_NH4_N_calculater": $("#Adjust_NH4_N_calculater").val(),
            "Adjust_NH4_N_calculater_max": $("#Adjust_NH4_N_calculater_max").val()
        },
        // end -------------------------------------------------------------------------
        // TDS --------------------------------------------------------------------------
        "TDS_param":{
            "alam_TDS_lo": $("#alam_TDS_lo").val(),
            "status_conf_alam_TDS_lo": $("#status_conf_alam_TDS_lo").prop('checked'),
            "alam_TDS_lolo": $("#alam_TDS_lolo").val(),
            "status_conf_alam_TDS_lolo": $("#status_conf_alam_TDS_lolo").prop('checked'),
            "alam_TDS_hi": $("#alam_TDS_hi").val(),
            "status_conf_alam_TDS_hi": $("#status_conf_alam_TDS_hi").prop('checked'),
            "alam_TDS_hihi": $("#alam_TDS_hihi").val(),
            "status_conf_alam_TDS_hihi": $("#status_conf_alam_TDS_hihi").prop('checked'),
            "gauge_TDS":{
                "name": $("#gauge_TDS_name").val(),
                "min": $("#gauge_TDS_min").val(),
                "max": $("#gauge_TDS_max").val(),
                "unit": $("#gauge_TDS_unit").val(),
            },
            "Adjust_TDS_calculater": $("#Adjust_TDS_calculater").val(),
            "Adjust_TDS_calculater_max": $("#Adjust_TDS_calculater_max").val()
        },
        // end -------------------------------------------------------------------------
        // Temperature --------------------------------------------------------------------------
        "Temperature_param":{
            "alam_Temperature_lo": $("#alam_Temperature_lo").val(),
            "status_conf_alam_Temperature_lo": $("#status_conf_alam_Temperature_lo").prop('checked'),
            "alam_Temperature_lolo": $("#alam_Temperature_lolo").val(),
            "status_conf_alam_Temperature_lolo": $("#status_conf_alam_Temperature_lolo").prop('checked'),
            "alam_Temperature_hi": $("#alam_Temperature_hi").val(),
            "status_conf_alam_Temperature_hi": $("#status_conf_alam_Temperature_hi").prop('checked'),
            "alam_Temperature_hihi": $("#alam_Temperature_hihi").val(),
            "status_conf_alam_Temperature_hihi": $("#status_conf_alam_Temperature_hihi").prop('checked'),
            "gauge_Temperature":{
                "name": $("#gauge_Temperature_name").val(),
                "min": $("#gauge_Temperature_min").val(),
                "max": $("#gauge_Temperature_max").val(),
                "unit": $("#gauge_Temperature_unit").val(),
            },
            "Adjust_Temperature_calculater": $("#Adjust_Temperature_calculater").val(),
            "Adjust_Temperature_calculater_max": $("#Adjust_Temperature_calculater_max").val()
        },
        // Salinity --------------------------------------------------------------------------
        "Salinity_param":{
            "alam_Salinity_lo": $("#alam_Salinity_lo").val(),
            "status_conf_alam_Salinity_lo": $("#status_conf_alam_Salinity_lo").prop('checked'),
            "alam_Salinity_lolo": $("#alam_Salinity_lolo").val(),
            "status_conf_alam_Salinity_lolo": $("#status_conf_alam_Salinity_lolo").prop('checked'),
            "alam_Salinity_hi": $("#alam_Salinity_hi").val(),
            "status_conf_alam_Salinity_hi": $("#status_conf_alam_Salinity_hi").prop('checked'),
            "alam_Salinity_hihi": $("#alam_Salinity_hihi").val(),
            "status_conf_alam_Salinity_hihi": $("#status_conf_alam_Salinity_hihi").prop('checked'),
            "gauge_Salinity":{
                "name": $("#gauge_Salinity_name").val(),
                "min": $("#gauge_Salinity_min").val(),
                "max": $("#gauge_Salinity_max").val(),
                "unit": $("#gauge_Salinity_unit").val(),
            },
            "Adjust_Salinity_calculater": $("#Adjust_Salinity_calculater").val(),
            "Adjust_Salinity_calculater_max": $("#Adjust_Salinity_calculater_max").val()
        },
        // Line --------------------------------------------------------------------------
        "Line_param":{
            "Line_token": $("#Line_token").val(),
            "line_quality_wt_lolo": $("#line_quality_wt_lolo").val(),
            "line_quality_wt_lo": $("#line_quality_wt_lo").val(),
            "line_quality_wt_hi": $("#line_quality_wt_hi").val(),
            "line_quality_wt_hihi": $("#line_quality_wt_hihi").val(),
            "Line_lolo_mess": $("#Line_lolo_mess").val(),
            "Line_lo_mess": $("#Line_lo_mess").val(),
            "Line_hi_mess": $("#Line_hi_mess").val(),
            "Line_hihi_mess": $("#Line_hihi_mess").val(),
            "status_conf_alam_line_lolo": $("#status_conf_alam_line_lolo").prop('checked'),
            "status_conf_alam_line_lo": $("#status_conf_alam_line_lo").prop('checked'),
            "status_conf_alam_line_hi": $("#status_conf_alam_line_hi").prop('checked'),
            "status_conf_alam_line_hihi": $("#status_conf_alam_line_hihi").prop('checked'),
        },


        // end -------------------------------------------------------------------------
        "dashboard_config_show_hide":{
            "pH" : $("#pH_show_hide").prop('checked'),
            "DO": $("#DO_show_hide").prop('checked'),
            "EC": $("#EC_show_hide").prop('checked'),
            "Turbidity": $("#Turbidity_show_hide").prop('checked'),
            "NH4_N": $("#NH4_N_show_hide").prop('checked'),
            "TDS": $("#TDS_show_hide").prop('checked'),
            "Temperature": $("#Temperature_show_hide").prop('checked'),
            "Spare": $("#Spare_show_hide").prop('checked'),
            "Salinity_show_hide": $("#Salinity_show_hide").prop('checked'),
        }
    }
    tranfer_data.send(JSON.stringify(json_obj));
    location.replace("dashboard.php");
}
function up(id){
    var num = $("#"+id).val()
    num++;
    $("#"+id).val(num);
}
function down(id){
    var num = $("#"+id).val()
    num--;
    $("#"+id).val(num);
}
function export__(){
    this_usbexport.send(1);
}
function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
</script>
</html>
