<!-- <!doctype html> -->
<html lang="en">
<head>
    <title>PICO</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bsadmin.css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="./keypad/jkeyboard-master/lib/css/jkeyboard.css">
</head>
<style>
    body{
        font-family: "Noto Sans Thai", 'Arimo', ystem-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Fira Sans", "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 1.5rem;
        color: #212121;
    }
    .pico-content{
        width: 1090px;
        height: 100%;
        background-color:#ffffff;
        position: relative;
        top: 0px;
        left: 10px;
        z-index: 1;
    }
    .pico-setting{
        font-size: 40px;
    }
    .pico-node-red-status{
        position: fixed;
        font-size:30px;
        right: 150px;
        top: 10px;
        z-index:3;
        color:rgb(47, 116, 8);
    }
    #keyboard{
        position: fixed;
        left: 0px;
        bottom: 0px;
        background: #747373;
        z-index: 5;
        padding: 10px;
        border-radius: 6px;
    }

</style>
<body style="background-color:rgb(255, 255, 255)">
<nav class="navbar navbar-expand navbar-dark bg-dark" style = "font-size: 30px;">
    <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>

    <a class="navbar-brand" href="#"></a>
    <!-- <div class='pico-node-red-status'><i class="fa fa-circle"> <p class='pico-text-status'>Active  </p></i></div> -->
</nav>

<div class="d-flex" style = "height:1000px;">
    <nav class="sidebar bg-dark toggled">
        <ul class="list-unstyled">
            <li><a href="dashboard.html"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-fw fa-table"></i> Record</a></li>
            <li><a href="#"><i class="fa fa-fw fa-hand-o-up"></i> Control</a></li>
            <li><a href="settings.html"><i class="fa fa-fw fa-gears"></i> Setting</a></li>
        </ul>
    </nav>
    <div class='pico-content'>
        <div class='content p-4'>

            <div class='row p-4'>
                <div class='col-sm-12'>
                    <center>
                        <div id = 'title'class='pico-setting'>
                            Setting
                        </div>
                    </center>
                </div>
            </div>

            <div class='row'>
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Title  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" onfocus="show_keyboard('title_')" id = "title_" style = "padding: 12px;" />
                </div>
            </div>
            <hr>
            <div class='row p-4'>
                <div class='col-sm-12'>
                    <center>
                        <div id = 'title'class='pico-setting'>
                            Time
                        </div>
                    </center>
                </div>
            </div>
            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Sampling  Time : </b>
                    </div>
                </div>
                <div class='col-md-2'>
                    <input  type = "number" class="form-control" disabled id = "number_" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('number_')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('number_')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Pump Run : </b>
                    </div>
                </div>
                <div class='col-md-2'>
                    <input  type = "number" class="form-control" disabled id = "pump_run_" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('pump_run_')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('pump_run_')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Delay : </b>
                    </div>
                </div>
                <div class='col-md-2'>
                    <input  type = "number" class="form-control" disabled id = "delay_" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('delay_')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('delay_')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
            </div>
            <!-- config gauge_ph -->
            <hr>
            <div class='row p-4'>
                <div class='col-sm-12'>
                    <center>
                        <div id = 'title'class='pico-setting'>
                            Set Param pH
                        </div>
                    </center>
                </div>
            </div>
            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Alarm pH [LO] : </b>
                    </div>
                </div>
                <!-- <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "number" class="form-control" id = "alam_pH_lo" style = "padding: 12px;" />
                </div> -->

                <div class='col-md-2'>
                    <input  type = "number" class="form-control" disabled id = "alam_pH_lo" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('alam_pH_lo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('alam_pH_lo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Alarm pH [LOLO] : </b>
                    </div>
                </div>
                <!-- <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "number" class="form-control" id = "alam_pH_lolo" style = "padding: 12px;" />
                </div> -->
                <div class='col-md-2'>
                    <input  type = "number" class="form-control" disabled id = "alam_pH_lolo" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('alam_pH_lolo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('alam_pH_lolo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
            </div>


            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Alarm pH [HI] : </b>
                    </div>
                </div>
                <!-- <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "number" class="form-control" id = "alam_pH_hi" style = "padding: 12px;" />
                </div> -->
                <div class='col-md-2'>
                    <input  type = "number" class="form-control" disabled id = "alam_pH_hi" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('alam_pH_hi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('alam_pH_hi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
            </div>


            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Alarm pH [HIHI] : </b>
                    </div>
                </div>
                <!-- <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "number" class="form-control" id = "alam_pH_hihi" style = "padding: 12px;" />
                </div> -->

                <div class='col-md-2'>
                    <input  type = "number" class="form-control" disabled id = "alam_pH_hihi" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('alam_pH_hihi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('alam_pH_hihi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>


            </div>

            <hr>
            <div class='row p-4'>
                <div class='col-sm-12'>
                    <center>
                        <div id = 'title' class='pico-setting'>
                            Set pH Gauge
                        </div>
                    </center>
                </div>
            </div>

            <div class='row'>
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Name  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" onfocus="show_keyboard('gauge_ph_name')" id = "gauge_ph_name" style = "padding: 12px;" />
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>min : </b>
                    </div>
                </div>
                <div class='col-md-2'>
                    <input  type = "number" class="form-control" disabled id = "gauge_ph_min" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('gauge_ph_min')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_ph_min')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>max : </b>
                    </div>
                </div>
                <div class='col-md-2'>
                    <input  type = "number" class="form-control" disabled id = "gauge_ph_max" style = "padding: 12px;text-align: center;font-size: 20px;" />
                </div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "up('gauge_ph_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
                    <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_ph_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
                </div>
            </div>
        

            <!-- <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>color leval 1  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" value = "" type = "text" class="form-control" id = "gauge_ph_lv1" style = "padding: 12px;" />
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>color leval 2  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" value = ""type = "text" class="form-control" id = "gauge_ph_lv2" style = "padding: 12px;" />
                </div>
            </div>

            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>color leval 3  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" value = "" type = "text" class="form-control" id = "gauge_ph_lv3" style = "padding: 12px;" />
                </div>
            </div> -->

            <div class='row' style = "margin-top:20px;" >
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>unit  : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input style = "padding: 12px;text-align: center;font-size: 20px;" value = " " type = "text" class="form-control" onfocus="show_keyboard('gauge_ph_unit')" id = "gauge_ph_unit" style = "padding: 12px;" />
                </div>
            </div>
            <!-- end config gauge_ph -->


            <div class='row' style = "margin-top:20px;">
                <div class='col-md-3'></div>
                <div class='col-md-3'></div>
                <div class='col-md-2'>
                    <button type = 'button' class="btn btn-primary" onclick = "onclick_()"style = "width: 100%;font-size:24px;"> Save </button>
                </div>
            </div>

            <hr>
            <!-- export csv -->
            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>Export .csv</div>
                <div class='col-md-4'>
                    <button type = 'button' class="btn btn-primary" onclick = "export__()"style = "width: 100%;font-size:24px;"> Export </button>
                </div>
            </div>
            <!-- end -->
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bsadmin.js"></script>


<!-- keypad -->
<script src="./keypad/jkeyboard-master/lib/js/jkeyboard.js"></script>


    <div id="keyboard"></div>
    <!-- <div id = "keyboard_out"></div> -->

</body>
<script type="text/javascript"> 
let tranfer_data = new WebSocket("ws://192.168.2.42:1880/wt/config");
let this_onload = new WebSocket("ws://192.168.2.42:1880/wt/onload");
let this_usbexport = new WebSocket("ws://192.168.2.42:1880/wt/usbexport");

// let tranfer_data = new WebSocket("ws://192.168.2.42:1880/wt/config");
// let this_onload = new WebSocket("ws://192.168.2.42:1880/wt/onload");

$( document ).ready(function() {
    onload_data();
    sock_to_update();
    function_usb_export();
});
function sock_to_update(){
    tranfer_data.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    tranfer_data.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.result){
            alert("บันทึกข้อมูลเรียบร้อยแล้ว")
        }
    };

    tranfer_data.onclose = function(event) {
    if (event.wasClean) {
        alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        alert('[close] Connection died');
        location.reload();
    }
    };

    tranfer_data.onerror = function(error) {
        alert(`[error] ${error.message}`);
        location.reload();
    };
}

function function_usb_export(){
    this_usbexport.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    this_usbexport.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.usb_tranf.code == 0){
            alert("Export .csv [ OK ]")
        }else{
            alert("Export Error")
        }
    };

    this_usbexport.onclose = function(event) {
    if (event.wasClean) {
        alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        alert('[close] Connection died');
        location.reload();
    }
    };

    this_usbexport.onerror = function(error) {
        alert(`[error] ${error.message}`);
        location.reload();
    };
}

function onload_data(){
    this_onload.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
        request_data();
    };

    this_onload.onmessage = function(event) {
        var obj = JSON.parse(event.data);
        console.log(obj);
        $("#title_").val(obj[0].title)
        $("#number_").val(obj[0].water_pump_time)
        $("#alam_pH_lo").val(obj[0].alam_pH_lo)
        $("#alam_pH_lolo").val(obj[0].alam_pH_lolo)
        $("#alam_pH_hi").val(obj[0].alam_pH_hl)
        $("#alam_pH_hihi").val(obj[0].alam_pH_hihi)
        // gauge ph
        $("#gauge_ph_name").val(obj[0].gauge_ph[0].name)
        $("#gauge_ph_min").val(obj[0].gauge_ph[0].min)
        $("#gauge_ph_max").val(obj[0].gauge_ph[0].max)
        // $("#gauge_ph_lv1").val(obj[0].gauge_ph[0].color_level[0])
        // $("#gauge_ph_lv2").val(obj[0].gauge_ph[0].color_level[1])
        // $("#gauge_ph_lv3").val(obj[0].gauge_ph[0].color_level[2])
        $("#gauge_ph_unit").val(obj[0].gauge_ph[0].unit)
        $("#delay_").val(obj[0].delay)
        $("#pump_run_").val(obj[0].pump_run)
        // end gauge ph



// $("#unit_pH").val(obj[0].unit_pH)
// $("#unit_pH").val(obj[0].unit_pH)
// $("#unit_DO").val(obj[0].unit_DO)
// $("#unit_EC").val(obj[0].unit_EC)
// $("#unit_Turbidity").val(obj[0].unit_Turbidity)
// $("#unit_Ammonium").val(obj[0].unit_Ammonium)
// $("#unit_TDS").val(obj[0].unit_TDS)
// $("#unit_Salinity").val(obj[0].unit_Salinity)

// unit_pH
// unit_pH
// unit_DO
// unit_EC
// unit_Turbidity
// unit_Ammonium
// unit_TDS
// unit_Salinity
    };

    this_onload.onclose = function(event) {
    if (event.wasClean) {
        alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        alert('[close] Connection died');
        location.reload();
    }
    };

    this_onload.onerror = function(error) {
        alert(`[error] ${error.message}`);
        location.reload();
    };

}

function request_data(){
    setTimeout(function() {
    }, 1000);
    this_onload.send('{"cmd":"onload"}');
}

function onclick_(){
    if ($("#number_").val() == "" || $("#title_").val() == ""){
        alert("กรอกข้อมูลให้ครบ")
    }else{
        // tranfer_data.send('{"_id":1,"water_pump_time":'+$("#number_").val()+',"min_is":23,"max_is":124,"set_temp":1000,"title":"'+$("#title_").val()+'","alam_pH_lo":"'+$("#alam_pH_lo").val()+'","alam_pH_lolo":"'+$("#alam_pH_lolo").val()+'","alam_pH_hl":"'+$("#alam_pH_hi").val()+'","alam_pH_hihi":"'+$("#alam_pH_hihi").val()+'","gauge_ph":[{"name":"'+$("#gauge_ph_name").val()+'","min":'+$("#gauge_ph_min").val()+',"max":'+$("#gauge_ph_max").val()+',"color_level":["'+$("#gauge_ph_lv1").val()+'","'+$("#gauge_ph_lv2").val()+'","'+$("#gauge_ph_lv3").val()+'"],"unit":"'+$("#gauge_ph_unit").val()+'"}]}');
        tranfer_data.send('{ "delay":'+$("#delay_").val()+',"pump_run":'+$("#pump_run_").val()+',   "_id":1,"water_pump_time":'+$("#number_").val()+',"min_is":23,"max_is":124,"set_temp":1000,"title":"'+$("#title_").val()+'","alam_pH_lo":'+$("#alam_pH_lo").val()+',"alam_pH_lolo":'+$("#alam_pH_lolo").val()+',"alam_pH_hl":'+$("#alam_pH_hi").val()+',"alam_pH_hihi":'+$("#alam_pH_hihi").val()+',"gauge_ph":[{"name":"'+$("#gauge_ph_name").val()+'","min":'+$("#gauge_ph_min").val()+',"max":'+$("#gauge_ph_max").val()+',"unit":"'+$("#gauge_ph_unit").val()+'"}]}');
    }

    // "unit_TDS":"'+$("unit_TDS").val()+'","unit_Salinity":"'+$("#title_").val()+'","unit_pH":"'+$("#unit_pH").val()+'","unit_pH":"'+$("#unit_pH").val()+'","unit_DO":"'+$("#unit_DO").val()+'","unit_EC":"'+$("#unit_EC").val()+'","unit_Turbidity":"'+$("#unit_Turbidity").val()+'","unit_TDS":"'+$("#unit_TDS").val()+'",
    //     "unit_Salinity":"'+$("#unit_Salinity").val()+'",
}
function up(id){
    var num = $("#"+id).val()
    num++;
    $("#"+id).val(num);
}
function down(id){
    var num = $("#"+id).val()
    if(num != 0){
        num--;
    }
    $("#"+id).val(num);
}
function export__(){
    this_usbexport.send(1);
}
function show_keyboard(input_id){
    // $("#keyboard_out").show();
    $("#keyboard").css("left","0px")
    $('#keyboard').jkeyboard({
        layout: "english",
        input: $('#'+input_id),
        // customLayouts: {
        //     selectable: ["english_capital"],
        //     english_capital: [
        //         ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0',],
        //         ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P',],
        //         ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L',],
        //         ['Z', 'X', 'C', 'V', 'B', 'N', 'M', '\'', '.'],
        //         ['space', '-', 'backspace']
        //     ],
        // }
    });
}
function hide_keyboard(){
    // $("#keyboard").hide();
}
</script>
</html>

            <!-- <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Unit [pH] : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input type = "text" class="form-control" id = "unit_pH" style = "padding: 12px;" />
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Unit [pH] : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input type = "text" class="form-control" id = "unit_pH" style = "padding: 12px;" />
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Unit [DO] : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input type = "text" class="form-control" id = "unit_DO" style = "padding: 12px;" />
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Unit [EC] : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input type = "text" class="form-control" id = "unit_EC" style = "padding: 12px;" />
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Unit [Turbidity] : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input type = "text" class="form-control" id = "unit_Turbidity" style = "padding: 12px;" />
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Unit [Ammonium] : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input type = "text" class="form-control" id = "unit_Ammonium" style = "padding: 12px;" />
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Unit [TDS] : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input type = "text" class="form-control" id = "unit_TDS" style = "padding: 12px;" />
                </div>
            </div>

            <div class='row' style = "margin-top:20px;">
                <div class='col-md-4'>
                    <div style = "padding:10px 0px 0px 20px;">
                        <b>Unit [Salinity] : </b>
                    </div>
                </div>
                <div class='col-md-4'>
                    <input type = "text" class="form-control" id = "unit_Salinity" style = "padding: 12px;" />
                </div>
            </div> -->
