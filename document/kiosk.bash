#!/bin/bash
sleep 1m

xset s noblank
xset s off
xset -dpms

unclutter -idle 0.5 -root &

DISPLAY=:0 /usr/bin/chromium-browser  --enable-native-gpu-memory-buffers --noerrdialogs --incognito --force-device-scale-factor=0.9 --disable-infobars --kiosk http://127.0.0.1/dashboard.php &
while true; do
	sleep 15
done
