<!-- <!doctype html> -->
<html lang="en">
<head>
    <title>PICO</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bsadmin.css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="./keypad/jkeyboard-master/lib/css/jkeyboard.css">
    <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.min.css" />
</head>
<style>
    body{
        font-family: "Noto Sans Thai", 'Arimo', ystem-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Fira Sans", "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 1.5rem;
        color: #212121;
    }
    .pico-content{
        width: 1090px;
        height: 100%;
        background-color:#ffffff;
        position: relative;
        top: 0px;
        left: 10px;
        z-index: 1;
    }
    .pico-dashboard{
        /* padding: 0px 0px 20px 500px; */
        font-size: 40px;
    }
    .pico-node-red-status{
        position: fixed;
        font-size:30px;
        right: 150px;
        top: 10px;
        z-index:3;
        color:rgb(47, 116, 8);
    }
    .pico-text-status{
        position: fixed;
        right: 90px;
        top: 14px;
        font-size: 20px;
        text-align: right;
    }
    .active-color {
        color: #2f9605;
        -webkit-animation-name: acive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: acive_co;
        animation-duration: 0.5s;
    }

    .inactive-color {
        color: #ff0000;
        -webkit-animation-name: inacive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: inacive_co;
        animation-duration: 0.5s;
    }
    .box-active {
        color: #000000;
        padding-top : 3px;
        padding-left : 10px;
        padding-right : 10px;
        border-radius : 4px;
        opacity: 0.9;
        background-color: #2f9605;
        -webkit-animation-name: box_atcive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: box_atcive_co;
        animation-duration: 0.5s;
    }
    @-webkit-keyframes box_atcive_co {
        from {background-color: #2f9605;}
        to {background-color: #50ac17;}
    }

    @keyframes box_atcive_co {
        from {background-color: #2f9605;}
        to {background-color: #50ac17;}
    }
    .box-inactive {
        color: #dd0000;
        padding-top : 3px;
        padding-left : 10px;
        padding-right : 10px;
        border-radius : 4px;
        opacity: 0.9;
        background-color: #8d8b8c;
        -webkit-animation-name: box_inatcive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: box_inatcive_co;
        animation-duration: 0.5s;
    }
        @-webkit-keyframes box_inatcive_co {
        from {background-color: #aca9aa;}
        to {background-color: #8d8b8c;}
    }

    @keyframes box_inatcive_co {
        from {background-color: #aca9aa;}
        to {background-color: #8d8b8c;}
    }
    @-webkit-keyframes acive_co {
        from {color: #2f9605;}
        to {color: #50ac17;}
    }

    @keyframes acive_co {
        from {color: #2f9605;}
        to {color: #50ac17;}
    }
    @-webkit-keyframes inacive_co {
        from {color: #aca9aa;}
        to {color: #8d8b8c;}
    }

    @keyframes inacive_co {
        from {color: #aca9aa;}
        to {color: #8d8b8c;}
    }
</style>

<body style="background-color:rgb(255, 255, 255)">
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bsadmin.js"></script>
<script type="text/javascript" src="button-toggle/js/bootstrap2-toggle.min.js"></script>
<script src="./keypad/jkeyboard-master/lib/js/jkeyboard.js"></script>
<nav class="navbar navbar-expand navbar-dark bg-dark" style = "font-size: 30px;">
    <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>

    <a class="navbar-brand" href="#"></a>
    <!-- <div class='pico-node-red-status'><i class="fa fa-circle"> <p class='pico-text-status'>Active  </p></i></div> -->
</nav>

<div class="d-flex" style = "height:1000px;">
    <nav class="sidebar bg-dark toggled">
        <ul class="list-unstyled">
            <li><a href="dashboard.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
            <li><a href="tables.php"><i class="fa fa-fw fa-table"></i> Record</a></li>
            <li><a href="control.php"><i class="fa fa-fw fa-hand-o-up"></i> Control</a></li>
            <li><a href="settings.php"><i class="fa fa-fw fa-gears"></i> Setting</a></li>
            <li><a href="login/index_security.php"><i class="fa fa-fw fa-lock"></i> Security</a></li>
            <li><a href="safemode.php"><i class="fa fa-fw fa-hand-o-up"></i> Safe Mode</a></li>
        </ul>
        </ul>
    </nav>
    <div class='pico-content'>
        <div class='content'>

            <div class='row'>
                <div class='col-sm-12'>
                    <center>
                        <div class='pico-dashboard'>
                            Control Pump
                        </div>
                    </center>
                </div>
            </div>
            <br><br>
            <div class='row'>
                <div class='col-sm-12'>
                    <div class="container-fluid" id="head_t">
                        <div class='row'>
                            <div class='col-sm-4'>
                                Pump 
                            </div>
                            <div class='col-sm-3'>
                            <div class='checkbox'>
                                <label onclick = "cpump('pump_control')">
                                    <input checked type='checkbox'  value = '1' id = 'pump_control'>
                                </label>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class='row'>
                <div class='col-sm-12'>
                    <div class="container-fluid" id="head_t">
                        <div class='row'>
                            <div class='col-sm-4'>
                            Purge Air
                            </div>
                            <div class='col-sm-3'>
                            <div class='checkbox'>
                                <label onclick = "cpump('purge_control')">
                                    <input checked type='checkbox'  value = '1' id = 'purge_control'>
                                </label>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
<script type="text/javascript"> 

let tranfer_data = new WebSocket("ws://localhost:1880/wt/config");
let this_onload = new WebSocket("ws://localhost:1880/wt/onload");
let pagelogin = new WebSocket("ws://localhost:1880/wt/pagelogin");
let data_wt = new WebSocket("ws://localhost:1880/wt/data_wt");
let read_onload = new WebSocket("ws://localhost:1880/wt/rload")
let control = new WebSocket("ws://localhost:1880/wt/control")
// let tranfer_data = new WebSocket("ws://localhost:1880/wt/config");
// let this_onload = new WebSocket("ws://localhost:1880/wt/onload");

$( document ).ready(function() {
    setCookie("page", "settings.php", 1);
    if (getCookie("username") == ""){
        location.replace("login/index.php");
    }
    onload_data();
    sock_to_update();
    page_login();
    fc_read_onload();
    control_pg();
    $('#pump_control').bootstrapToggle({on: 'ON',off: 'OFF'});
    $('#purge_control').bootstrapToggle({on: 'ON',off: 'OFF'});
    document.body.style.zoom = "90%";
    // read_onload.send(1);
});
function cpump(id){
    console.log($('#'+id).prop('checked'));
    var out = {
        "id":id,
        "data":$('#'+id).prop('checked')
    }
    control.send(JSON.stringify(out));
}
function control_pg(){
    control.onopen = function(e) {
        console.log("[open] Connection established, send -> server is read_onload control");
    };

    control.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        console.log(obj);
    };

    control.onclose = function(event) {
        if (event.wasClean) {
            // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
            location.reload();
        } else {
            // alert('[close] Connection died');
            location.reload();
        }
    };

    control.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}
function fc_read_onload(){
    read_onload.onopen = function(e) {
        console.log("[open] Connection established, send -> server is read_onload");
        read_onload.send(1);
    };

    read_onload.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        console.log(obj);
        if (obj[0]){
            $('#pump_control').bootstrapToggle('on')
        }else{
            $('#pump_control').bootstrapToggle('off')
        }
        if (obj[1]){
            $('#purge_control').bootstrapToggle('on')
        }else{
            $('#purge_control').bootstrapToggle('off')
        }
        // $("#status_conf_alam_TDS_hihi").prop('checked'),
    };

    read_onload.onclose = function(event) {
        if (event.wasClean) {
            // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
            location.reload();
        } else {
            // alert('[close] Connection died');
            location.reload();
        }
    };

    read_onload.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}
function page_login(){
    pagelogin.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    pagelogin.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.page_login){
            location.replace("login/index.php");
        }
    };

    pagelogin.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    pagelogin.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}
function sock_to_update(){
    tranfer_data.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    tranfer_data.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.result){
            // alert("บันทึกข้อมูลเรียบร้อยแล้ว")
        }
    };

    tranfer_data.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    tranfer_data.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}

function onload_data(){
    this_onload.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
        request_data();
    };

    this_onload.onmessage = function(event) {
        var obj = JSON.parse(event.data);
        console.log(obj);
        $("#title").text(obj[0].title);
    };

    this_onload.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    this_onload.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };

}

function request_data(){
    setTimeout(function() {
    }, 1000);
    this_onload.send('{"cmd":"onload"}');
    
}

function onclick_(){
    if ($("#number_").val() == "" || $("#title_").val() == ""){
        alert("กรอกข้อมูลให้ครบ")
    }else{
        // tranfer_data.send('{"_id":1,"water_pump_time":'+$("#number_").val()+',"min_is":23,"max_is":124,"set_temp":1000,"title":"'+$("#title_").val()+'","alam_Temperature_lo":"'+$("#alam_Temperature_lo").val()+'","alam_Temperature_lolo":"'+$("#alam_Temperature_lolo").val()+'","alam_Temperature_hl":"'+$("#alam_Temperature_hi").val()+'","alam_Temperature_hihi":"'+$("#alam_Temperature_hihi").val()+'"}');
    }
}
function up(id){
    var num = $("#"+id).val()
    num++;
    $("#"+id).val(num);
}
function down(id){
    var num = $("#"+id).val()
    if(num != 0){
        num--;
    }
    $("#"+id).val(num);
}
function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
</script>
</html>