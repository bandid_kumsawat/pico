<!-- <!doctype html> -->
<html lang="en">
<head>
    <title>PICO</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bsadmin.css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<style>
    body{
        font-family: "Noto Sans Thai", 'Arimo', ystem-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Fira Sans", "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 1.5rem;
        color: #212121;
    }
    .pico-content{
        width: 1090px;
        height: 600px;
        background-color:#ffffff;
        position: relative;
        top: 0px;
        left: 10px;
        z-index: 1;
    }
    .pico-dashboard{
        /* padding: 0px 0px 20px 500px; */
        font-size: 30px;
        font-weight:900;
        padding:30px 0px 30px 0;
    }
    .pico-node-red-status{
        position: fixed;
        font-size:30px;
        right: 150px;
        top: 10px;
        z-index:3;
        color:rgb(47, 116, 8);
    }
    .pico-text-status{
        position: fixed;
        right: 90px;
        top: 14px;
        font-size: 20px;
        text-align: right;
    }
    /*  this is gauge */
    .outer {
        width: 270px;
        height: 200px;
        margin: 0;
    }
    .outer .chart-container {
        width: 270px;
        float: left;
        height: 200px;
    }
    .highcharts-yaxis-grid .highcharts-grid-line {
        display: none;
    }

    @media (max-width: 600px) {
        .outer {
            width: 100%;
            height: 400px;
        }
        .outer .chart-container {
            width: 300px;
            float: none;
            margin: 0 auto;
        }

    }
    .active-color {
        color: #2f9605;
        -webkit-animation-name: acive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: acive_co;
        animation-duration: 0.5s;
    }

    .inactive-color {
        color: #8d8b8c;
        -webkit-animation-name: inacive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: inacive_co;
        animation-duration: 0.5s;
    }
    .box-active {
        color: #000000;
        padding-top : 3px;
        padding-left : 10px;
        padding-right : 10px;
        border-radius : 4px;
        opacity: 0.9;
        background-color: #2f9605;
        -webkit-animation-name: box_atcive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: box_atcive_co;
        animation-duration: 0.5s;
    }
    @-webkit-keyframes box_atcive_co {
        from {background-color: #2f9605;}
        to {background-color: #50ac17;}
    }

    @keyframes box_atcive_co {
        from {background-color: #2f9605;}
        to {background-color: #50ac17;}
    }
    .box-inactive {
        color: #dd0000;
        padding-top : 3px;
        padding-left : 10px;
        padding-right : 10px;
        border-radius : 4px;
        /* opacity: 0.9; */
        background-color: #8d8b8c;
        -webkit-animation-name: box_inatcive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: box_inatcive_co;
        animation-duration: 0.5s;
    }
    @-webkit-keyframes box_inatcive_co {
        from {background-color: #8d8b8c;}
        to {background-color: #8d8b8c;}
    }

    @keyframes box_inatcive_co {
        from {background-color: #8d8b8c;}
        to {background-color: #8d8b8c;}
    }


    @-webkit-keyframes acive_co {
        from {color: #2f9605;}
        to {color: #50ac17;}
    }
    @keyframes acive_co {
        from {color: #2f9605;}
        to {color: #50ac17;}
    }


    @-webkit-keyframes inacive_co {
        from {color: #8d8b8c;}
        to {color: #8d8b8c;}
    }
    @keyframes inacive_co {
        from {color: #8d8b8c;}
        to {color: #8d8b8c;}
    }

    .pump_status_cl{
        position: absolute;
        right: 20px;
        top: 20px;
    }

.row .bot {
  	background-color: #aeaeae;
	padding-top: 5px;
	padding-bottom: 10px;
	width: 100%;
	
}
</style>
<body style="background-color:rgb(255, 255, 255)">

<nav class="navbar navbar-expand navbar-dark bg-dark" style = "font-size: 30px;">
    <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>

    <a class="navbar-brand" href="#"></a>
    <!-- <div class='pico-node-red-status'><i class="fa fa-circle"> <p class='pico-text-status'>Active  </p></i></div> -->
</nav>

<div class="d-flex">
    <nav class="sidebar bg-dark toggled">
        <ul class="list-unstyled">
            <li><a href="dashboard.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
            <li><a href="tables.php"><i class="fa fa-fw fa-table"></i> Record</a></li>
            <li><a href="control.php"><i class="fa fa-fw fa-hand-o-up"></i> Control</a></li>
            <li><a href="settings.php"><i class="fa fa-fw fa-gears"></i> Setting</a></li>
            <li><a href="login/index_security.php"><i class="fa fa-fw fa-lock"></i> Security</a></li>
            <li><a href="safemode.php"><i class="fa fa-fw fa-hand-o-up"></i> Safe Mode</a></li>
        </ul>
    </nav>
    <div class='pico-content'>
        <div class='content'>

             <div class='row'>
                <div class='col-sm-12'>
                    <center>
                        <div id = 'title'class='pico-dashboard'>
                            
                        </div>
                    </center>
                </div>
            </div>

            <div class='row'>
                <div class='col-sm-3'>

                        <div class="outer">
                            <div id="container-pH" class="chart-container">
                                <center>
                                    <div id='pH_name' style = "font-size: 30px;font-weight:700;color: #000000;">pH</div>
                                    <div id='pH_value' style = "font-size: 45px;font-weight:900;color: #468DDF;padding-top: 20px;">0</div>
                                    <div id='pH_unit' style = "font-size: 30px;font-weight:700;color: #474747;padding-top: 10px;"></div>
                                <center>
                            </div>
                        </div>

                </div>

                <div class='col-sm-3'>

                    <div class="outer">
                        <div id="container-DO" class="chart-container">
                            <center>
                                <div id='DO_name' style = "font-size: 30px;font-weight:700;color: #000000;">DO</div>
                                <div id='DO_value' style = "font-size: 45px;font-weight:900;color: #468DDF;padding-top: 20px;">0</div>
                                <div id='DO_unit' style = "font-size: 30px;font-weight:700;color: #474747;padding-top: 10px;"></div>
                            </center>
                        </div>
                    </div>

                </div>

                <div class='col-sm-3'>

                    <div class="outer">
                        <div id="container-EC" class="chart-container">
                            <center>
                                <div id='EC_name' style = "font-size: 30px;font-weight:700;color: #000000;">EC</div>
                                <div id='EC_value' style = "font-size: 45px;font-weight:900;color: #468DDF;padding-top: 20px;">0</div>
                                <div id='EC_unit' style = "font-size: 30px;font-weight:700;color: #474747;padding-top: 10px;"></div>
                            </center>
                        </div>
                    </div>

                </div>


                <div class='col-sm-3'>

                    <div class="outer">
                        <div id="container-Turbidity" class="chart-container">
                            <center>
                                <div id='Turbidity_name' style = "font-size: 30px;font-weight:700;color: #000000;">Turbidity</div>
                                <div id='Turbidity_value' style = "font-size: 45px;font-weight:900;color: #468DDF;padding-top: 20px;">0</div>
                                <div id='Turbidity_unit' style = "font-size: 30px;font-weight:700;color: #474747;padding-top: 10px;"></div>
                            </center>
                        </div>
                    </div>

                </div>


            </div>

            <div class='row' style = "padding-top: 40px;">

                <div class='col-sm-3'>
                    <div class="outer">
                        <div id="container-NH4_N" class="chart-container">
                            <center>
                                <div id='NH4_N_name' style = "font-size: 30px;font-weight:700;color: #000000;">NH4-N</div>
                                <div id='NH4_N_value' style = "font-size: 45px;font-weight:900;color: #468DDF;padding-top: 20px;">0</div>
                                <div id='NH4_N_unit' style = "font-size: 30px;font-weight:700;color: #474747;padding-top: 10px;"></div>
                            </center>
                        </div>
                    </div>
                </div>

                <div class='col-sm-3'>
                    <div class="outer">
                        <div id="container-TDS" class="chart-container">
                            <center>
                                <div id='TDS_name' style = "font-size: 30px;font-weight:700;color: #000000;">TDS</div>
                                <div id='TDS_value' style = "font-size: 45px;font-weight:900;color: #468DDF;padding-top: 20px;">0</div>
                                <div id='TDS_unit' style = "font-size: 30px;font-weight:700;color: #474747;padding-top: 10px;"></div>
                            </center>
                        </div>
                    </div>
                </div>

                <div class='col-sm-3'>
                    <div class="outer">
                        <div id="container-Temperature" class="chart-container">
                            <center>
                                <div id='Temperature_name' style = "font-size: 30px;font-weight:700;color: #000000;">Temperature</div>
                                <div id='Temperature_value' style = "font-size: 45px;font-weight:900;color: #468DDF;padding-top: 20px;">0</div>
                                <div id='Temperature_unit' style = "font-size: 30px;font-weight:700;color: #474747;padding-top: 10px;"></div>
                            </center>
                        </div>
                    </div>
                </div>

                <div class='col-sm-3'>
                    <div class="outer">
                        <div id="container-Salinity" class="chart-container">
                            <center>
                                <div id='Salinity_name' style = "font-size: 30px;font-weight:700;color: #000000;">Salinity</div>
                                <div id='Salinity_value' style = "font-size: 45px;font-weight:900;color: #468DDF;padding-top: 20px;">0</div>
                                <div id='Salinity_unit' style = "font-size: 30px;font-weight:700;color: #474747;padding-top: 10px;"></div>
                            </center>
                        </div>
                    </div>
                </div>

            </div>

            <div class='row'>
		<div class='col-sm-12'><center>
			<span id = "ma_status"></span>

                        <span id = "pump_status"></span>

                        <span id = "ovl_status"></span>

                        <span id = "ovt_status"></span>

                        <span id = "purge_status"></span>

                        <span id = "door_status"></span>

                        <span id = "secure_status"></span>

		</center></div>

            </div>

        </div>
    </div>
</div>
<!-- hight chart gauge -->

<!-- <script src="js/gauge/highcharts.js"></script>
<script src="js/gauge/highcharts-more.js"></script> -->
<!-- <script src="js/gauge/exporting.js"></script> -->
<!-- <script src="js/gauge/solid-gauge.js"></script> -->


<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bsadmin.js"></script>
</body>
<script type="text/javascript"> 

let tranfer_data = new WebSocket("ws://localhost:1880/wt/config");
let this_onload = new WebSocket("ws://localhost:1880/wt/onload");
let pagelogin = new WebSocket("ws://localhost:1880/wt/pagelogin");
let read_onload = new WebSocket("ws://localhost:1880/wt/rload")
let data_modbus = new WebSocket("ws://localhost:1880/wt/data_gauge");

// let check_page_on = new WebSocket("ws://localhost:1880/wt/online");

var dataValue = 0

$( document ).ready(function() {
    
    onload_data();
    sock_to_update();
    page_login();
    recive_modbus_data();
    fc_read_onload();
    setInterval(function(){ 
        read_onload.send(1);
    }, 5000);
    // show value in gauge
    
    document.body.style.zoom = "90%";
});
function fc_read_onload(){
    read_onload.onopen = function(e) {
        console.log("[open] Connection established, send -> server is read_onload");
        read_onload.send(1);
    };

    read_onload.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        console.log(obj);
        if (obj[0] == true){
            $('#pump_status').html("");
            $('#pump_status').append("<button type='button' class='btn btn-success'>Pump Run</button>");
        }else{
            $('#pump_status').html("");
            $('#pump_status').append("<button type='button' class='btn btn-danger'>Pump Off</button>");
        }
        if (obj[1] == true){
            $('#ovl_status').html("");
            $('#ovl_status').append("<button type='button' class='btn btn-danger'>Over Load</button>");
        }else{
            $('#ovl_status').html(""); 
            $('#ovl_status').append("<button type='button' class='btn btn-secondary'>Over Load</button>");
        }


        if (obj[2] == true){
            $('#ovt_status').html("");
            $('#ovt_status').append("<button type='button' class='btn btn-primary'>Over Flow</button>");
        }else{
            $('#ovt_status').html(""); 
            $('#ovt_status').append("<button type='button' class='btn btn-secondary'>Over Flow</button>");
        }


        if (obj[3] == true){
            $('#purge_status').html("");
            $('#purge_status').append("<button type='button' class='btn btn-info'>Purge Run</button>");
        }else{
            $('#purge_status').html(""); 
            $('#purge_status').append("<button type='button' class='btn btn-secondary'>Purge Off</button>");
        }



        if (obj[4] == true){
            $('#door_status').html("");
            $('#door_status').append("<button type='button' class='btn btn-danger'>Open Door</button>");
        }else{
            $('#door_status').html(""); 
            $('#door_status').append("<button type='button' class='btn btn-secondary'>Close Door</button>");
        }



        if (obj[5]  == true){
            $('#secure_status').html("");
            $('#secure_status').append("<button type='button' class='btn btn-success'>Security On</button>");
        }else{
            $('#secure_status').html(""); 
            $('#secure_status').append("<button type='button' class='btn btn-danger'>Security Off</button>");
        }


       if (obj[6]  == "R"){
            $('#ma_status').html("");
            $('#ma_status').append("<button type='button' class='btn btn-danger'>คุณภาพน้ำเสียวิกฤตหนัก </button>");
        }else if (obj[6]  == "Y"){
            $('#ma_status').html(""); 
            $('#ma_status').append("<button type='button' class='btn btn-warning'>คุณภาพน้ำเสียเฝ้าระวัง     </button>");
        }else{
            $('#ma_status').html(""); 
            $('#ma_status').append("<button type='button' class='btn btn-success'>คุณภาพน้ำปกติปลอดภัย    </button>");
        }





        // if (obj[1]){
        //     $('#purge_control').bootstrapToggle('on')
        // }else{
        //     $('#purge_control').bootstrapToggle('off')
        // }
        // $("#status_conf_alam_TDS_hihi").prop('checked'),
    };

    read_onload.onclose = function(event) {
        if (event.wasClean) {
            // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
            
        } else {
            // alert('[close] Connection died');
            
        }
    };

    read_onload.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        
    };
}
function recive_modbus_data(){
    data_modbus.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    data_modbus.onmessage = function(event) {
        // console.log(event.data);
        var obj = JSON.parse(event.data);
        dataValue = obj
        console.log(dataValue);
    };

    data_modbus.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        
    } else {
        // alert('[close] Connection died');
        
    }
    };

    data_modbus.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        
    };
}
function page_login(){
    pagelogin.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    pagelogin.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.page_login){
            location.replace("login/index_security.php");
        }
    };

    pagelogin.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        
    } else {
        // alert('[close] Connection died');
        
    }
    };

    pagelogin.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        
    };
}
function sock_to_update(){
    tranfer_data.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    tranfer_data.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.result){
            // alert("บันทึกข้อมูลเรียบร้อยแล้ว")
        }
    };

    tranfer_data.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        
    } else {
        // alert('[close] Connection died');
        
    }
    };

    tranfer_data.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        
    };
}

function onload_data(){
    this_onload.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
        // this_onload.send('{"cmd":"onload"}');
        request_data();
    };

    this_onload.onmessage = function(event) {
        var obj = JSON.parse(event.data);
        console.log(obj);
        $("#title").text(obj[0].title);

        // generate value hear
        setInterval(function () {
        //     console.log(dataValue.pH);
        //     // PH
            $("#pH_name").text(obj[0].pH_param.gauge_ph.name);
            $("#pH_value").text(dataValue.pH);
            $("#pH_unit").text(obj[0].pH_param.gauge_ph.unit);

            // Turbidity
            $("#Turbidity_name").text(obj[0].Turbidity_param.gauge_Turbidity.name);
            $("#Turbidity_value").text(dataValue.Turbidity);
            $("#Turbidity_unit").text(obj[0].Turbidity_param.gauge_Turbidity.unit);

            // EC
            $("#EC_name").text(obj[0].EC_param.gauge_EC.name);
            $("#EC_value").text(dataValue.EC);
            $("#EC_unit").text(obj[0].EC_param.gauge_EC.unit);

            // DO
            $("#DO_name").text(obj[0].DO_param.gauge_DO.name);
            $("#DO_value").text(dataValue.DO);
            $("#DO_unit").text(obj[0].DO_param.gauge_DO.unit);

            // NH4_N
            $("#NH4_N_name").text(obj[0].NH4_N_param.gauge_NH4_N.name);
            $("#NH4_N_value").text(dataValue.NH4_N);
            $("#NH4_N_unit").text(obj[0].NH4_N_param.gauge_NH4_N.unit);

            // TDS
            $("#TDS_name").text(obj[0].TDS_param.gauge_TDS.name);
            $("#TDS_value").text(dataValue.TDS);
            $("#TDS_unit").text(obj[0].TDS_param.gauge_TDS.unit);

            // Temperature
            $("#Temperature_name").text(obj[0].Temperature_param.gauge_Temperature.name);
            $("#Temperature_value").text(dataValue.Temperature);
            $("#Temperature_unit").text(obj[0].Temperature_param.gauge_Temperature.unit);

            // Salinity
            $("#Salinity_name").text(obj[0].Salinity_param.gauge_Salinity.name);
            $("#Salinity_value").text(dataValue.Salinity);
            $("#Salinity_unit").text(obj[0].Salinity_param.gauge_Salinity.unit);

        }, 2000);

        if (!obj[0].dashboard_config_show_hide.pH){

            $( "#container-pH" ).attr( "hidden" , "hidden")

        }

        if (!obj[0].dashboard_config_show_hide.DO){

            $( "#container-DO" ).attr( "hidden" , "hidden")

        }

        if (!obj[0].dashboard_config_show_hide.EC){

            $( "#container-EC" ).attr( "hidden" , "hidden")

        }


        if (!obj[0].dashboard_config_show_hide.Turbidity){

            $( "#container-Turbidity" ).attr( "hidden" , "hidden")

        }

        if (!obj[0].dashboard_config_show_hide.NH4_N){

            $( "#container-NH4_N" ).attr( "hidden" , "hidden")

        }

        if (!obj[0].dashboard_config_show_hide.TDS){

            $( "#container-TDS" ).attr( "hidden" , "hidden")

        }

        if (!obj[0].dashboard_config_show_hide.Temperature){

            $( "#container-Temperature" ).attr( "hidden" , "hidden")

        }

        if (!obj[0].dashboard_config_show_hide.Spare){

            $( "#container-Spare" ).attr( "hidden" , "hidden")

        }

        if (!obj[0].dashboard_config_show_hide.Salinity_show_hide){

            $( "#container-Salinity" ).attr( "hidden" , "hidden")

        }

    };

    this_onload.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        
    } else {
        // alert('[close] Connection died');
        
    }
    };

    this_onload.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        
    };

}

function request_data(){
    setTimeout(function() {
    }, 1000);
    
    this_onload.send('{"cmd":"onload"}');
}

function onclick_(){
    if ($("#number_").val() == "" || $("#title_").val() == ""){
        alert("กรอกข้อมูลให้ครบ")
    }else{
        // tranfer_data.send('{"_id":1,"water_pump_time":'+$("#number_").val()+',"min_is":23,"max_is":124,"set_temp":1000,"title":"'+$("#title_").val()+'","alam_Temperature_lo":"'+$("#alam_Temperature_lo").val()+'","alam_Temperature_lolo":"'+$("#alam_Temperature_lolo").val()+'","alam_Temperature_hl":"'+$("#alam_Temperature_hi").val()+'","alam_Temperature_hihi":"'+$("#alam_Temperature_hihi").val()+'"}');
    }
}
function up(id){
    var num = $("#"+id).val()
    num++;
    $("#"+id).val(num);
}
function down(id){
    var num = $("#"+id).val()
    if(num != 0){
        num--;
    }
    $("#"+id).val(num);
}

// function page_online(){
//     check_page_on.onopen = function(e) {
//         console.log("connected")
//         check_page_on.send(1);
//     };

//     check_page_on.onmessage = function(event) {
//         var obj = JSON.parse(event.data);
//     };

//     check_page_on.onclose = function(event) {
//     if (event.wasClean) {
//         // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
//         
//     } else {
//         // alert('[close] Connection died');
//         
//     }
//     };

//     check_page_on.onerror = function(error) {
//         // alert(`[error] ${error.message}`);
//         
//     };

// }


</script>
</html>

<!-- 1564657195424 
1564656470255 024637

1564656470255024637   156465738545000000 


156465750576100000

1564657645813394489
-->
