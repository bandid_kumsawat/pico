<!-- <!doctype html> -->
<html lang="en">
<head>
    <title>PICO</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bsadmin.css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="./keypad/jkeyboard-master/lib/css/jkeyboard.css">
    <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.min.css" />
</head>
<style>
    body{
        font-family: "Noto Sans Thai", 'Arimo', ystem-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Fira Sans", "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 1.5rem;
        color: #212121;
    }
    .pico-content{
        width: 1090px;
        height: 100%;
        background-color:#ffffff;
        position: relative;
        top: 0px;
        left: 10px;
        z-index: 1;
    }
    .pico-dashboard{
        /* padding: 0px 0px 20px 500px; */
        font-size: 40px;
    }
    .pico-node-red-status{
        position: fixed;
        font-size:30px;
        right: 150px;
        top: 10px;
        z-index:3;
        color:rgb(47, 116, 8);
    }
    .pico-text-status{
        position: fixed;
        right: 90px;
        top: 14px;
        font-size: 20px;
        text-align: right;
    }
    .active-color {
        color: #2f9605;
        -webkit-animation-name: acive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: acive_co;
        animation-duration: 0.5s;
    }

    .inactive-color {
        color: #ff0000;
        -webkit-animation-name: inacive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: inacive_co;
        animation-duration: 0.5s;
    }
    .box-active {
        color: #000000;
        padding-top : 3px;
        padding-left : 10px;
        padding-right : 10px;
        border-radius : 4px;
        opacity: 0.9;
        background-color: #2f9605;
        -webkit-animation-name: box_atcive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: box_atcive_co;
        animation-duration: 0.5s;
    }
    @-webkit-keyframes box_atcive_co {
        from {background-color: #2f9605;}
        to {background-color: #50ac17;}
    }

    @keyframes box_atcive_co {
        from {background-color: #2f9605;}
        to {background-color: #50ac17;}
    }
    .box-inactive {
        color: #dd0000;
        padding-top : 3px;
        padding-left : 10px;
        padding-right : 10px;
        border-radius : 4px;
        opacity: 0.9;
        background-color: #8d8b8c;
        -webkit-animation-name: box_inatcive_co; 
        -webkit-animation-duration: 0.5s; 
        animation-name: box_inatcive_co;
        animation-duration: 0.5s;
    }
        @-webkit-keyframes box_inatcive_co {
        from {background-color: #aca9aa;}
        to {background-color: #8d8b8c;}
    }

    @keyframes box_inatcive_co {
        from {background-color: #aca9aa;}
        to {background-color: #8d8b8c;}
    }
    @-webkit-keyframes acive_co {
        from {color: #2f9605;}
        to {color: #50ac17;}
    }

    @keyframes acive_co {
        from {color: #2f9605;}
        to {color: #50ac17;}
    }
    @-webkit-keyframes inacive_co {
        from {color: #aca9aa;}
        to {color: #8d8b8c;}
    }

    @keyframes inacive_co {
        from {color: #aca9aa;}
        to {color: #8d8b8c;}
    }

    .toggle{
        width: 70px !important;
        height: 40px !important;
    }

    .status_di_off{
        font-size: 30px;
        padding-top:22px !important;
        color: #808080
        /* position: relative; */
    }
    .status_di_on{
        font-size: 30px;
        padding-top:22px !important;
        color: #00B900
        /* position: relative; */
    }
</style>

<body style="background-color:rgb(255, 255, 255)">
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bsadmin.js"></script>
<script type="text/javascript" src="button-toggle/js/bootstrap2-toggle.min.js"></script>
<script src="./keypad/jkeyboard-master/lib/js/jkeyboard.js"></script>
<nav class="navbar navbar-expand navbar-dark bg-dark" style = "font-size: 30px;">
    <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>

    <a class="navbar-brand" href="#"></a>
    <!-- <div class='pico-node-red-status'><i class="fa fa-circle"> <p class='pico-text-status'>Active  </p></i></div> -->
</nav>

<div class="d-flex" style = "height:1000px;">
    <nav class="sidebar bg-dark toggled">
        <ul class="list-unstyled">
            <li><a href="dashboard.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
            <li><a href="tables.php"><i class="fa fa-fw fa-table"></i> Record</a></li>
            <li><a href="control.php"><i class="fa fa-fw fa-hand-o-up"></i> Control</a></li>
            <li><a href="settings.php"><i class="fa fa-fw fa-gears"></i> Setting</a></li>
            <li><a href="login/index_security.php"><i class="fa fa-fw fa-lock"></i> Security</a></li>
            <li><a href="safemode.php"><i class="fa fa-fw fa-hand-o-up"></i> Safe Mode</a></li>
        </ul>
        </ul>
    </nav>
    <div class='pico-content'>
        <div class='content'>

            <div class='row'>
                <div class='col-sm-12'>
                    <center>
                        <div class='pico-dashboard'>
                            Safe Mode
                        </div>
                    </center>
                </div>
            </div>
            <br><br>
                <div class='row'>
                    <div class='col-sm-12'>
                        <table class="table table-bordered">
                            <thead>
                                <tr align = 'center'>
                                <th style = "padding-top:20px !important;font-size:26px;font-weight: 750;" colspan="2">Digital Ouput</th>
                                <th style = "padding-top:20px !important;font-size:26px;font-weight: 750;" colspan="2">Digital Input</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- for DI and DO -- 1 -->
                                <tr align = 'center'>
                                    <!-- DO -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DO1</td>
                                    <td style = "font-size: 30px;padding-top:20px !important;">
                                        <div class='checkbox'>
                                            <label id = 'DO1_click'>
                                                <input checked type='checkbox'  value = '1' id = 'DO1'>
                                            </label>
                                        </div>
                                    </td>

                                    <!-- DI -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DI1</td>
                                    <td id = "DI1" class='status_di_off'>
                                        <span id = ""><i class="fa fa-circle"></i></span>
                                    </td>
                                </tr>


                                <!-- for DI and DO -- 2 -->
                                <tr align = 'center'>
                                    <!-- DO -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DO2</td>
                                    <td style = "font-size: 30px;padding-top:20px !important;">
                                        <div class='checkbox'>
                                            <label id = 'DO2_click'>
                                                <input checked type='checkbox'  value = '1' id = 'DO2'>
                                            </label>
                                        </div>
                                    </td>

                                    <!-- DI -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DI2</td>
                                    <td id = "DI2" class='status_di_off'>
                                        <span id = ""><i class="fa fa-circle"></i></span>
                                    </td>
                                </tr>


                                <!-- for DI and DO -- 3 -->
                                <tr align = 'center'>
                                    <!-- DO -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DO3</td>
                                    <td style = "font-size: 30px;padding-top:20px !important;">
                                        <div class='checkbox'>
                                            <label id = 'DO3_click'>
                                                <input checked type='checkbox'  value = '1' id = 'DO3'>
                                            </label>
                                        </div>
                                    </td>

                                    <!-- DI -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DI3</td>
                                    <td id = "DI3" class='status_di_off'>
                                        <span id = ""><i class="fa fa-circle"></i></span>
                                    </td>
                                </tr>



                                <!-- for DI and DO -- 4 -->
                                <tr align = 'center'>
                                    <!-- DO -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DO4</td>
                                    <td style = "font-size: 30px;padding-top:20px !important;">
                                        <div class='checkbox'>
                                            <label id = 'DO4_click'>
                                                <input checked type='checkbox'  value = '1' id = 'DO4'>
                                            </label>
                                        </div>
                                    </td>

                                    <!-- DI -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DI4</td>
                                    <td id = "DI4" class='status_di_off'>
                                        <span id = ""><i class="fa fa-circle"></i></span>
                                    </td>
                                </tr>

                                <!-- for DI and DO -- 5 -->
                                <tr align = 'center'>
                                    <!-- DO -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DO5</td>
                                    <td style = "font-size: 30px;padding-top:20px !important;">
                                        <div class='checkbox'>
                                            <label id = 'DO5_click'>
                                                <input checked type='checkbox'  value = '1' id = 'DO5'>
                                            </label>
                                        </div>
                                    </td>

                                    <!-- DI -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DI5</td>
                                    <td id = "DI5" class='status_di_off'>
                                        <span id = ""><i class="fa fa-circle"></i></span>
                                    </td>
                                </tr>



                                <!-- for DI and DO -- 6 -->
                                <tr align = 'center'>
                                    <!-- DO -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DO6</td>
                                    <td style = "font-size: 30px;padding-top:20px !important;">
                                        <div class='checkbox'>
                                            <label id = 'DO6_click'>
                                                <input checked type='checkbox'  value = '1' id = 'DO6'>
                                            </label>
                                        </div>
                                    </td>

                                    <!-- DI -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DI6</td>
                                    <td id = "DI6" class='status_di_off'>
                                        <span id = ""><i class="fa fa-circle"></i></span>
                                    </td>
                                </tr>

                                <!-- for DI and DO -- 7 -->
                                <tr align = 'center'>
                                    <!-- DO -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DO7</td>
                                    <td style = "font-size: 30px;padding-top:20px !important;">
                                        <div class='checkbox'>
                                            <label id = 'DO7_click'>
                                                <input checked type='checkbox'  value = '1' id = 'DO7'>
                                            </label>
                                        </div>
                                    </td>

                                    <!-- DI -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DI7</td>
                                    <td id = "DI7" class='status_di_off'>
                                        <span id = ""><i class="fa fa-circle"></i></span>
                                    </td>
                                </tr>


                                <!-- for DI and DO -- 8 -->
                                <tr align = 'center'>
                                    <!-- DO -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DO8</td>
                                    <td style = "font-size: 30px;padding-top:20px !important;">
                                        <div class='checkbox'>
                                            <label id = 'DO8_click'>
                                                <input checked type='checkbox'  value = '1' id = 'DO8'>
                                            </label>
                                        </div>
                                    </td>

                                    <!-- DI -->
                                    <td style = "padding-top:20px !important;font-size:24px;font-weight: 700;">DI8</td>
                                    <td id = "DI8" class='status_di_off'>
                                        <span id = ""><i class="fa fa-circle"></i></span>
                                    </td>
                                </tr>

                            </tbody>
                            
                        </table>
                    </div>
                </div>
            <br><br>
        </div>
    </div>
</div>


</body>
<script type="text/javascript"> 

$( document ).ready(function() {
    setCookie("page", "settings.php", 1);
    if (getCookie("username") == ""){
        // location.replace("login/index.php");
    }


    // for DO1
    $('#DO1').bootstrapToggle({on: 'ON',off: 'OFF'});
    checkbox_control("DO1",1);
    // for DO2
    $('#DO2').bootstrapToggle({on: 'ON',off: 'OFF'});
    checkbox_control("DO2",2);
    // for DO3
    $('#DO3').bootstrapToggle({on: 'ON',off: 'OFF'});
    checkbox_control("DO3",3);
    // for DO4
    $('#DO4').bootstrapToggle({on: 'ON',off: 'OFF'});
    checkbox_control("DO4",4);
    // for DO5
    $('#DO5').bootstrapToggle({on: 'ON',off: 'OFF'});
    checkbox_control("DO5",5);
    // for DO6
    $('#DO6').bootstrapToggle({on: 'ON',off: 'OFF'});
    checkbox_control("DO6",6);
    // for DO7
    $('#DO7').bootstrapToggle({on: 'ON',off: 'OFF'});
    checkbox_control("DO7",7);
    // for DO8
    $('#DO8').bootstrapToggle({on: 'ON',off: 'OFF'});
    checkbox_control("DO8",8);




    document.body.style.zoom = "90%";
    request_data_io(1);

    // read DI interval 4 + 1 sec.
    // in node-red read DO - wait 1 sec - DI 
    setInterval(function(){ request_data_io(3) }, 4000);
});

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
function checkbox_control(id,addr){
    $('#'+id+'_click').click(function () {
        var cf = confirm("คุณต้องการสั่ง "+id+" ทำงานไหม");
        if (cf){
            if (!$('#'+id).prop('checked')){
                $('#'+id).bootstrapToggle('off')
            }else{
                $('#'+id).bootstrapToggle('on')
            }
            console.log("data : " + !$('#'+id).prop('checked'))
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://127.0.0.1:1880/safemode?id="+addr+"&value=" + ($('#'+id).prop('checked') ? 0 : 1),
                "method": "GET",
                "headers": {
                    "Accept": "*/*",
                    "Cache-Control": "no-cache",
                    "cache-control": "no-cache"
                }
            }

            $.ajax(settings).done(function (response) {
                console.log(response);
            });
        }else{
            if ($('#'+id).prop('checked')){
                $('#'+id).bootstrapToggle('off')
            }else{
                $('#'+id).bootstrapToggle('on')
            }
        }
    });
}
/*
    *************************
    *
    *design by Bandid Kumsawat
    *
    *************************
    request_data_io(int mode);
    mode -> select di and do
        1   all
        2   DO
        3   DI
    fc = 1 = do
    fc = 2 = di
*/
function request_data_io(mode){
    try{
        let promise1 = new Promise((resolve, reject) => {
            var settings1 = {
                "async": true,
                "crossDomain": true,
                "url": "http://127.0.0.1:1880/getstatusdodi",
                "method": "GET",
                "headers": {
                    "Accept": "*/*",
                    "Cache-Control": "no-cache",
                    "cache-control": "no-cache"
                }
            }
            $.ajax(settings1).done(function (response) {
                console.log(response)
                console.log(response.DO.data.length)
                if (mode == 1){

                    // this is DO
                    for (var i = 0;i < response.DO.data.length;i++){
                        if (!response.DO.data[i]){
                            $('#DO'+(i + 1)).bootstrapToggle('off')
                        }else{
                            $('#DO'+(i + 1)).bootstrapToggle('on')
                        }
                    }


                    // this is DI
                    for (var i = 0;i < response.DI.data.length;i++){
                        if (response.DI.data[i]){
                            document.getElementById("DI" + (i+1)).setAttribute('class','status_di_on')
                        }else{
                            document.getElementById("DI" + (i+1)).setAttribute('class','status_di_off')
                        }
                    }


                }else if (mode == 2){
                    // this is DO
                    console.log("DO");
                    for (var i = 0;i < response.DO.data.length;i++){
                        if (!response.DO.data[i]){
                            $('#DO'+(i + 1)).bootstrapToggle('off')
                        }else{
                            $('#DO'+(i + 1)).bootstrapToggle('on')
                        }
                    }
                }else if (mode == 3){
                    // this is DI
                    console.log("DI");
                    for (var i = 0;i < response.DI.data.length;i++){
                        console.log(response.DI.data[i])
                        if (response.DI.data[i]){
                            // $("#DI" + (i+1)).addClass('status_di_on');
                            document.getElementById("DI" + (i+1)).setAttribute('class','status_di_on')
                        }else{
                            // $("#DI" + (i+1)).addClass('status_di_off');
                            document.getElementById("DI" + (i+1)).setAttribute('class','status_di_off')
                        }
                    }
                }else{
                    return null
                }
            });
        });

        let result1 = promise1;
    } catch (err){
        console.log("this is error from mode" + err)
    }


    // this di
    // var settings1 = {
    //     "async": true,
    //     "crossDomain": true,
    //     "url": "http://127.0.0.1:1880/getstatusdodi?fc=2",
    //     "method": "GET",
    //     "headers": {
    //         "Accept": "*/*",
    //         "Cache-Control": "no-cache",
    //         "cache-control": "no-cache"
    //     }
    // }

    // $.ajax(settings1).done(function (response1) {
    //     console.log("DI");
    //     console.log(response1);
    //     for (var i = 0;i < response1.data.length;i++){
    //         if (response1.data[i]){
    //             $("#DI1").addClass('status_di_on');
    //             console.log('on')
    //         }else{
    //             $("#DI1").addClass('status_di_off');
    //             console.log('off')
    //         }
    //     }
    // });


        // // this do
        // var settings = {
        //     "async": true,
        //     "crossDomain": true,
        //     "url": "http://127.0.0.1:1880/getstatusdodi?fc=1",
        //     "method": "GET",
        //     "headers": {
        //         "Accept": "*/*",
        //         "Cache-Control": "no-cache",
        //         "cache-control": "no-cache"
        //     }
        // }

        // $.ajax(settings).done(function (response) {
        //     console.log("DO");
        //     console.log(response);
        // });
}
</script>
</html>