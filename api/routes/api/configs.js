var router = require('express').Router();
var mongoose = require('mongoose');
var config = mongoose.model('config');

router.post('/data', function(req, res, next) {
    console.log(req.body);
    config.update({ id: req.body.id },{$set:{
        water_pump_time: req.body.water_pump_time,
        min_is: req.body.min_is,
        max_is: req.body.max_is,
        set_temp: req.body.set_temp
    }
    },{upsert:true},function (err,count){return res.send(count)})
});

module.exports = router;