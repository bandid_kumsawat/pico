var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({
    station_id:String, 
    des_PT:String,
    dec_TT:String,
    dec_Load_cell:String,
    dec_Gas_Gun:String,
    dec_FT:String
});
mongoose.model('Station', SensorSchema);