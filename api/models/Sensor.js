var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({
    station_id:Number, 
    PT:String,
    TT:String,
    Load_cell:String,
    Gas_Gun:String,
    FT:String,
});

mongoose.model('Sensor', SensorSchema);