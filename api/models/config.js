var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({
    id:Number, 
    water_pump_time:Number,
    min_is:Number,
    max_is:Number,
    set_temp:Number
});

mongoose.model('config', SensorSchema);