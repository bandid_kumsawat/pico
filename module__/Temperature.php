
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config gauge_Temperature <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title'class='pico-setting'>
                Set Param Temperature
            </div>
        </center>
    </div>
</div>


<div  hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Temperature [LOLO] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_Temperature_lolo" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Temperature-1"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Temperature_lolo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Temperature_lolo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input chTemperatureked type='checkbox'  value = '1' id = 'status_conf_alam_Temperature_lolo'>
            </label>
        </div>
    </div>
</div>

<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Temperature [LO] : </b>
        </div>
    </div>

    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_Temperature_lo" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Temperature-2"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Temperature_lo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Temperature_lo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>

    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input chTemperatureked type='checkbox'  value = '1' id = 'status_conf_alam_Temperature_lo'>
            </label>
        </div>
    </div>

    
</div>
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Temperature [HI] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_Temperature_hi" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Temperature-3"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Temperature_hi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Temperature_hi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input chTemperatureked type='checkbox'  value = '1' id = 'status_conf_alam_Temperature_hi'>
            </label>
        </div>
    </div>
</div>


<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Temperature [HIHI] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_Temperature_hihi" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Temperature-4"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Temperature_hihi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Temperature_hihi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input chTemperatureked type='checkbox'  value = '1' id = 'status_conf_alam_Temperature_hihi'>
            </label>
        </div>
    </div>
</div>
<!-- -------------------------------------------------------- -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div  class='pico-setting'>
                Set Adjust Temperature Calculater
            </div>
        </center>
    </div>
</div>
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>value min: </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  value =  "800" type = "text" class="form-control"  id = "Adjust_Temperature_calculater" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Temperature-5"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('Adjust_Temperature_calculater')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('Adjust_Temperature_calculater')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>value max: </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  value =  "4000" type = "text" class="form-control"  id = "Adjust_Temperature_calculater_max" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Temperature-6"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('Adjust_Temperature_calculater_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('Adjust_Temperature_calculater_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>
<hr>
<!-- -------------------------------------------------------- -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title' class='pico-setting'>
                Set Temperature Gauge
            </div>
        </center>
    </div>
</div>

<div class='row'>
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Name  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" id = "gauge_Temperature_name" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-Temperature-gauge-name"/>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>min : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "gauge_Temperature_min" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Temperature-7"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('gauge_Temperature_min')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_Temperature_min')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>max : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "gauge_Temperature_max" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Temperature-8"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('gauge_Temperature_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_Temperature_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>

<div class='row' style = "margin-top:20px;" >
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>unit  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" value = " " type = "text" class="form-control" id = "gauge_Temperature_unit" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-Temperature-gauge-unit"/>
    </div>
</div>
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config gauge_Temperature <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->

<script type="text/javascript"> 
    $( document ).ready(function() {
        $('#status_conf_alam_Temperature_lo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_Temperature_lolo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_Temperature_hi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_Temperature_hihi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#keyboard_Temperature_gauge_name').jkeyboard({
            // layout: "english",
            input: $('#gauge_Temperature_name'),
        });
        $('#keyboard_Temperature_gauge_unit').jkeyboard({
            // layout: "english",
            input: $('#gauge_Temperature_unit'),
        });

        // ============================================================
        // ============================================================
        // ============================================================
        // ============================================================
        $('#keyboard_Temperature_1').jkeyboard({
            input: $('#alam_Temperature_lolo'),
        });
        $('#keyboard_Temperature_2').jkeyboard({
            input: $('#alam_Temperature_lo'),
        });
        $('#keyboard_Temperature_3').jkeyboard({
            input: $('#alam_Temperature_hi'),
        });
        $('#keyboard_Temperature_4').jkeyboard({
            input: $('#alam_Temperature_hihi'),
        });
        $('#keyboard_Temperature_5').jkeyboard({
            input: $('#Adjust_Temperature_calculater'),
        });
        $('#keyboard_Temperature_6').jkeyboard({
            input: $('#Adjust_Temperature_calculater_max'),
        });
        $('#keyboard_Temperature_7').jkeyboard({
            input: $('#gauge_Temperature_min'),
        });
        $('#keyboard_Temperature_8').jkeyboard({
            input: $('#gauge_Temperature_max'),
        });
        // ============================================================
        // ============================================================
        // ============================================================
        // ============================================================
    });
</script>
