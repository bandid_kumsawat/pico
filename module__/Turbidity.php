
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> config gauge_Turbidity <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title'class='pico-setting'>
                Set Param Turbidity
            </div>
        </center>
    </div>
</div>


<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Turbidity [LOLO] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_Turbidity_lolo" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Turbidity-1" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Turbidity_lolo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Turbidity_lolo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div  hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input chTurbidityked type='checkbox'  value = '1' id = 'status_conf_alam_Turbidity_lolo'>
            </label>
        </div>
    </div>
</div>

<div hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Turbidity [LO] : </b>
        </div>
    </div>

    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_Turbidity_lo" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Turbidity-2"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Turbidity_lo')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Turbidity_lo')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>

    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input chTurbidityked type='checkbox'  value = '1' id = 'status_conf_alam_Turbidity_lo'>
            </label>
        </div>
    </div>

    
</div>
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Turbidity [HI] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_Turbidity_hi" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Turbidity-3"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Turbidity_hi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Turbidity_hi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div  hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input chTurbidityked type='checkbox'  value = '1' id = 'status_conf_alam_Turbidity_hi'>
            </label>
        </div>
    </div>
</div>


<div  hidden class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Alarm Turbidity [HIHI] : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "alam_Turbidity_hihi" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Turbidity-4"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('alam_Turbidity_hihi')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('alam_Turbidity_hihi')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
    <!-- status enable and disable -->
    <div hidden class = 'col-md-4'>
        <div class='checkbox'>
            <label>
                <input chTurbidityked type='checkbox'  value = '1' id = 'status_conf_alam_Turbidity_hihi'>
            </label>
        </div>
    </div>
</div>
<!-- -------------------------------------------------------- -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div  class='pico-setting'>
                Set Adjust Turbidity Calculater
            </div>
        </center>
    </div>
</div>
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>value min : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  value =  "800" type = "text" class="form-control"  id = "Adjust_Turbidity_calculater" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Turbidity-5"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('Adjust_Turbidity_calculater')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('Adjust_Turbidity_calculater')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>
<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>value max : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  value =  "4000" type = "text" class="form-control"  id = "Adjust_Turbidity_calculater_max" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Turbidity-6"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('Adjust_Turbidity_calculater_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('Adjust_Turbidity_calculater_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>
<hr>
<!-- -------------------------------------------------------- -->
<hr>
<div class='row p-4'>
    <div class='col-sm-12'>
        <center>
            <div id = 'title' class='pico-setting'>
                Set Turbidity Gauge
            </div>
        </center>
    </div>
</div>

<div class='row'>
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>Name  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" type = "text" class="form-control" id = "gauge_Turbidity_name" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-Turbidity-gauge-name"/>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>min : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "gauge_Turbidity_min" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Turbidity-7"/>
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('gauge_Turbidity_min')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_Turbidity_min')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>

<div class='row' style = "margin-top:20px;">
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>max : </b>
        </div>
    </div>
    <div class='col-md-2'>
        <input  type = "text" class="form-control"  id = "gauge_Turbidity_max" style = "padding: 12px;text-align: center;font-size: 20px;" data-toggle="modal" data-target=".bd-example-modal-lg-keyboard-Turbidity-8" />
    </div>
    <div class='col-md-2'>
        <button type = 'button' class="btn btn-primary" onclick = "up('gauge_Turbidity_max')" style = "width: 60px;font-size:24px;"> <b>+</b> </button>
        <button type = 'button'  class="btn btn-primary" onclick = "down('gauge_Turbidity_max')" style = "width: 60px;font-size:24px;"> <b>-</b> </button>
    </div>
</div>

<div class='row' style = "margin-top:20px;" >
    <div class='col-md-4'>
        <div style = "padding:10px 0px 0px 20px;">
            <b>unit  : </b>
        </div>
    </div>
    <div class='col-md-4'>
        <input style = "padding: 12px;text-align: center;font-size: 20px;" value = " " type = "text" class="form-control" id = "gauge_Turbidity_unit" style = "padding: 12px;" data-toggle="modal" data-target=".bd-example-modal-lg-Turbidity-gauge-unit"/>
    </div>
</div>
<!-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end config gauge_Turbidity <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< -->

<script type="text/javascript"> 
    $( document ).ready(function() {
        $('#status_conf_alam_Turbidity_lo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_Turbidity_lolo').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_Turbidity_hi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#status_conf_alam_Turbidity_hihi').bootstrapToggle({on: 'Enable',off: 'Disable'});
        $('#keyboard_Turbidity_gauge_name').jkeyboard({
            // layout: "english",
            input: $('#gauge_Turbidity_name'),
        });
        $('#keyboard_Turbidity_gauge_unit').jkeyboard({
            // layout: "english",
            input: $('#gauge_Turbidity_unit'),
        });

        // ============================================================
        // ============================================================
        // ============================================================
        // ============================================================
        $('#keyboard_Turbidity_1').jkeyboard({
            input: $('#alam_Turbidity_lolo'),
        });
        $('#keyboard_Turbidity_2').jkeyboard({
            input: $('#alam_Turbidity_lo'),
        });
        $('#keyboard_Turbidity_3').jkeyboard({
            input: $('#alam_Turbidity_hi'),
        });
        $('#keyboard_Turbidity_4').jkeyboard({
            input: $('#alam_Turbidity_hihi'),
        });
        $('#keyboard_Turbidity_5').jkeyboard({
            input: $('#Adjust_Turbidity_calculater'),
        });
        $('#keyboard_Turbidity_6').jkeyboard({
            input: $('#Adjust_Turbidity_calculater_max'),
        });
        $('#keyboard_Turbidity_7').jkeyboard({
            input: $('#gauge_Turbidity_min'),
        });
        $('#keyboard_Turbidity_8').jkeyboard({
            input: $('#gauge_Turbidity_max'),
        });

        
// alam_Turbidity_lolo
// alam_Turbidity_lo
// alam_Turbidity_hi
// alam_Turbidity_hihi


// Adjust_Turbidity_calculater
// Adjust_Turbidity_calculater_max
// gauge_Turbidity_min
// gauge_Turbidity_max
        // ============================================================
        // ============================================================
        // ============================================================
        // ============================================================
    });
</script>