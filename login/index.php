<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bsadmin.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

<!-- keypad -->
<link rel="stylesheet" href="../keypad/jkeyboard-master/lib/css/jkeyboard.css">
<!-- end -->


</head>
<style>
    #keyboard{
        position: fixed;
        background: #747373;
        padding: 10px;
        border-radius: 6px;
    }
    .modal-dialog {
        position:fixed;
        top:300px;
        right:auto;
        left:100px;
        bottom:0px;
    }
    .modal-lg {
        width: 820px;
        margin: auto;
    }
    .modal-backdrop
    {
        opacity:0 !important;
    }
</style>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-50 p-b-90">
					<span class="login100-form-title p-b-51">
						PICO
					</span>

					
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Username is required">
						<input id = "username"class="input100" type="text" name="username" placeholder="Username" data-toggle="modal" data-target=".bd-example-modal-lg-username">
						<span class="focus-input100"></span>
					</div>
					
					
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input id = "password"class="input100" type="password" name="pass" placeholder="Password" data-toggle="modal" data-target=".bd-example-modal-lg-password">
						<span class="focus-input100"></span>
					</div>

					<div class="container-login100-form-btn m-t-17">
						<button onclick = "login()" class="login100-form-btn">
							Login
						</button>
					</div>
					<div class="container-login100-form-btn m-t-3">
						<button onclick = "back()" class="login100-form-btn">
							Back
						</button>
					</div>

			</div>
		</div>
	</div>
	
	<div class="modal fade bd-example-modal-lg-password" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div id="keyboard_password"></div>
			</div>
		</div>
	</div>
	<div class="modal fade bd-example-modal-lg-username" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div id="keyboard_username"></div>
			</div>
		</div>
	</div>
	
	<div id="dropDownSelect1"></div>
	<!-- keypad -->
<!--===============================================================================================-->
	<!-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> -->
	
<script src="../js/jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script src="../keypad/jkeyboard-master/lib/js/jkeyboard.js"></script>
	<script>
		let sk_login = new WebSocket("ws://localhost:1880/wt/login");
		// let pagelogin = new WebSocket("ws://localhost:1880/wt/pagelogin");
		$( document ).ready(function() {
			connect();
			// page_login();
			// setCookie("username", 99, 1);
			// console.log(getCookie("username"));
			// if (getCookie("username") == ""){
			// 	location.replace("../settings.php");
			// }
			$('#keyboard_username').jkeyboard({
				// layout: "english",
				input: $('#username'),
			});
			$('#keyboard_password').jkeyboard({
				// layout: "english",
				input: $('#password'),
			});
		});
function login(){
	sk_login.send(1);
}
function page_login(){
    pagelogin.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    pagelogin.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
        if (obj.page_login){
            location.replace("login/index.php");
        }
    };

    pagelogin.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    pagelogin.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}
function connect(){
    sk_login.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    sk_login.onmessage = function(event) {
        var obj = JSON.parse(event.data);
		console.log(event);
		var chech = 1;
		for (var i = 0;i < obj[0].login.length;i++){
			var user = $("#username").val();
			var pass = $("#password").val();
			console.log(user)
			console.log(pass)
			console.log(obj[0].login[i].username)
			console.log(obj[0].login[i].password)
			if (user == obj[0].login[i].username && pass == obj[0].login[i].password){
				chech = 1;
				setCookie("username", obj[0].login[i].username, 1);
				setCookie("password", obj[0].login[i].password, 1);
				setCookie("type", obj[0].login[i].type, 1);
				location.replace("../" + getCookie("page"));
				break;
			}else{
				chech = 0;
			}
		}
		if (chech == 0){
			alert("Can't Login.");
		}
    };

    sk_login.onclose = function(event) {
		if (event.wasClean) {
			// alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
			location.reload();
		} else {
			// alert('[close] Connection died');
			location.reload();
		}
    };

    sk_login.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };

}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
function back(){
	location.replace("../dashboard.php");
}
	</script>
</body>
</html>