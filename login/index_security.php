<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bsadmin.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

<!-- keypad -->
<link rel="stylesheet" href="../keypad/jkeyboard-master/lib/css/jkeyboard.css">
<!-- end -->


</head>
<style>
    #keyboard{
        position: fixed;
        background: #747373;
        padding: 10px;
        border-radius: 6px;
    }
    .modal-dialog {
        position:fixed;
        top:55%;
        right:50%;
        left:45%;
        bottom:0px;
    }
    .modal-lg {
        width: 300px;
        margin: auto;
    }
    .modal-backdrop
    {
        opacity:0 !important;
    }
</style>
<body>
	<!-- keypad -->
<!--===============================================================================================-->
	<!-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> -->
	
<script src="../js/jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script src="../keypad/jkeyboard-master/lib/js/jkeyboard.js"></script>
	<script>
		let sk_login = new WebSocket("ws://localhost:1880/wt/clr_secu");
		//let pagelogin = new WebSocket("ws://localhost:1880/wt/login_secu");

		//let alert_line = new WebSocket("ws://localhost:1880/wt/alert_line");


		$(document ).ready(function() {
		

			//connect();
			//page_login();
			// setCookie("username", 99, 1);
			//console.log(getCookie("security"));

			var havecook = readCookie("security");

			if (havecook == 99){
			 	//location.replace("../dashboard.php");



  	var x1 = document.getElementById("inlog");
 	var x2 = document.getElementById("outlog");
  if (x1.style.display === "none") {
    x1.style.display = "block";
  } else {
    x1.style.display = "none";
  }
  if (x2.style.display === "none") {
    x2.style.display = "block";
  } else {
    x2.style.display = "none";
  }

			}

			$('#keyboard_username').jkeyboard({
				layout: "numbers_only",
				input: $('#username'),
			});

		});


function login(){
		var user = $("#username").val();

		if(user == '0000'){
			var chech = 1;
			createCookie("security", 99, 1);
			sk_login.send('Yes');
			//alert("Security is closed.");
			location.replace("../dashboard.php");

			
		}else{
			alert("Can't Security.");
			chech = 0;
		}

}
function config_alert_line(){
	alert_line.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    alert_line.onmessage = function(event) {
        console.log(event.data);
        var obj = JSON.parse(event.data);
         if (obj.page_login){
             location.replace("login/index_security.php");
         }
    };

    alert_line.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    alert_line.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}


function page_login(){
    pagelogin.onopen = function(e) {
        console.log("[open] Connection established, send -> server");
    };

    pagelogin.onmessage = function(event) {
        console.log(event.data);
    };

    pagelogin.onclose = function(event) {
    if (event.wasClean) {
        // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        location.reload();
    } else {
        // alert('[close] Connection died');
        location.reload();
    }
    };

    pagelogin.onerror = function(error) {
        // alert(`[error] ${error.message}`);
        location.reload();
    };
}




function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 *1000));
        var expires = "; expires=" + date.toGMTString();
    } else {
        var expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}


     
function closex(){
	eraseCookie("security");
	sk_login.send('No');
	//alert("Security is enable.");

	location.replace("../dashboard.php");
}

     
function back(){
	location.replace("../dashboard.php");
}


	</script>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-50 p-b-90">
					<span class="login100-form-title p-b-51">
						Security
					</span>

					<div id="inlog" style="display:block;">
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Pin code is required">
						<input id = "username" class="input100" type="text" name="username" placeholder="Pin Code" data-toggle="modal" data-target=".bd-example-modal-lg-username">
						<span class="focus-input100"></span>
					</div>
					
					
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input id = "password" class="input100" type="hidden" name="pass" placeholder="Password" data-toggle="modal" data-target=".bd-example-modal-lg-password">
						<span class="focus-input100"></span>
					</div>

					<div class="container-login100-form-btn m-t-17">
						<button onclick = "login()" class="login100-form-btn">
							Turn off security
						</button>
					</div>

					<div class="container-login100-form-btn m-t-3">
						<button onclick = "back()" class="login100-form-btn">
							Back
						</button>
					</div>
					</div>
					<div id="outlog" style="display:none;">
					
					<div class="container-login100-form-btn m-t-3">
						<button onclick = "closex()" class="login100-form-btn">
							Turn on security
						</button>
					</div>
					<div class="container-login100-form-btn m-t-3">
						<button onclick = "back()" class="login100-form-btn">
							Back
						</button>
					</div>
					</div>

			</div>
		</div>
	</div>
	
	<div class="modal fade bd-example-modal-lg-password" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div id="keyboard_password"></div>
			</div>
		</div>
	</div>
	<div class="modal fade bd-example-modal-lg-username" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div id="keyboard_username"></div>
			</div>
		</div>
	</div>
	
	<div id="dropDownSelect1"></div>

</body>
</html>